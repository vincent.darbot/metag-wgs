# metagWGS: Usage

## I. Basic usage

1. See [Installation page](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/installation.md) to install metagWGS. 

2. See [Functional tests](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/functional_tests/README.md) to download the test dataset(s).

3. Run a basic script:

   > The next script is a script working on **genologin slurm cluster**. Il allows to run the default [step](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/README.md#metagwgs-steps) `01_clean_qc` of the pipeline (without host reads deletion and taxonomic affiliation of reads).
   
   **WARNING:** You must adapt it if you want to run it into your cluster. You must install/load Nextflow and Singularity, and define a specific configuration for your cluster.

 * Write in a file `Script.sh`:

   > ```bash
   > #!/bin/bash
   > #SBATCH -p workq
   > #SBATCH --mem=6G
   > module purge
   > module load bioinfo/Nextflow-v20.01.0
   > module load system/singularity-3.5.3
   > nextflow run -profile test_genotoul_workq metagwgs/main.nf --reads "<PATH_TO_DATASET_INPUT>/*_{R1,R2}.fastq.gz" --skip_removal_host --skip_kaiju
   > ```

   > **NOTE:** you can change Nextflow and Singularity versions with other versions available on the cluster (see all versions with `search_module ToolName`). Nextflow version must be >= v20 and Singularity version must be >= v3.

 * Run `Script.sh` with this command line:
   > ```bash
   > sbatch Script.sh
   > ```

    See the description of output files in [this part](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/output.md) of the documentation and [there](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/functional_tests/README.md#iii-output)

    `Script.sh` is a basic script that requires only a small test data input and no other files. To analyze real data, in addition to your metagenomic whole genome shotgun `.fastq` files, you need to download different files which are described in the next chapter.

> **WARNING:** if you run metagWGS to **analyze real metagenomics data on genologin cluster**, you have to use the `unlimitq` queue to run your Nextflow script. To do this, instead of writing in the second line of your script `#SBATCH -p workq` you need to write `#SBATCH -p unlimitq`.

## II. Input files

### 1. General mandatory files

Launching metagWGS involves the use of mandatory files:
* The **metagenomic whole genome shotgun data** you want to analyze: `.fastq` or `.fastq.gz` R1 and R2 files (Illumina HiSeq3000 or NovaSeq sequencing, 2*150bp). For a cleaner MultiQC html report at the end of the pipeline, raw data with extensions `_R1` and `_R2` are preferred to those with extensions `_1` and `_2`.
* The **metagWGS.sif**, **eggnog_mapper.sif** and **mosdepth.sif** Singularity images (in `metagwgs/env` folder).

### 2. Mandatory files for certain steps

In addition to the general mandatory files, if you wish to launch certain steps of the pipeline, you will need previously generated or downloaded files:

* Step `01_clean_qc`, **only if you want to remove host reads**: you need a fasta file of the genome.

* Step `05_alignment` **(against a protein database)**: download the protein database you want to use. For example you can use NR database.

* Step `08_binning`, **taxonomic affiliation of bins**: you need to download CAT/BAT database with `wget tbb.bio.uu.nl/bastiaan/CAT_prepare/CAT_prepare_20210107.tar.gz`

**WARNINGS:**
- if you use step `02_assembly` or `03_filtering` or `04_structural_annot` or `05_alignment` or `06_func_annot` or `07_taxo_affi` or `08_binning` without skipping `01_clean_qc` or host reads removal, you need to use the mandatory files of step `01_clean_qc`.

**AND**

- if you use step `06_func_annot` or `07_taxo_affi` or `08_binning`, you need to use the mandatory files of step `05_alignment`.

### 3. Others files for certain steps

In addition to the `general mandatory files` and `mandatory files for certain steps`, if you wish to launch certain steps of the pipeline, you can download files before running metagWGS. It is not mandatory but it avoids unnecessary downloads.

* Step `01_clean_qc`:

    * **Only if you want to remove host reads**: if you also have the BWA index (.amb, .ann, .bwt, .pac and .sa files) of the host genome fasta file, you can specify it as a metagWGS parameter: `--host_bwa_index`.

    * **Only if you want to have the taxonomic affiliation of reads**: you can previously download kaiju database index [here](http://kaiju.binf.ku.dk/server) (blue insert on the left side, click right of the desired database -> copy the link address). For example, download `refseq 2020-05-25 (17GB)` with `wget http://kaiju.binf.ku.dk/database/kaiju_db_refseq_2020-05-25.tgz` and unpack it with `tar -zxvf kaiju_db_refseq_2020-05-25.tgz`. This file is not mandatory, a metagWGS parameter allows to download automatically the wanted database among all available in the [kaiju website](http://kaiju.binf.ku.dk/server). **WARNING:** you are not authorized to use kaiju database built with `kaiju-makedb` command line.

Analyzing your metagenomic data with metagWGS allows you to use all **`nextflow run` options** in your `nextflow run` command line and different **metagWGS specific parameters**. Some of these specific parameters are useful to indicate the `<PATH>` to these input files. The next chapters will explain these options and parameters.

## III. Nextflow options

**NOTE:** all `nextflow run` options available [here](https://www.nextflow.io/docs/latest/cli.html?highlight=trace#run) can be used when you run metagWGS. They are notified in the command line by `-`.

### 1. Mandatory option

#### `-profile`

It allows you to choose the configuration profile among:
   * `singularity` to analyze **your files** with metagWGS with **singularity containers**. You must have installed Singularity and downloaded the three Singularity containers associated to metagWGS (see [Installation page](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/installation.md)). Thus, your results will be reproducible. **NOTE:** there is no definition of the type of cluster (SGE, slurm, etc) you use in this profile.
   * `conda` to analyze **your files** with metagWGS with **conda environments** already defined. You must have installed Miniconda (see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)).
      * **NOTE 1:** use of `conda` profile is easier than `singularity` profile but your results will be less reproducible.
      * **NOTE 2:** there is no definition of the type of cluster (SGE, slurm, etc) you use in this profile. You can precise it inside of a `nextflow.config` file you can add into your working directory. For example if you are working on a slurm cluster, add this line to your `nextflow.config`:
       ```bash
       process.executor = 'slurm'
       ```
      * **NOTE 3:** on [genologin cluster](http://bioinfo.genotoul.fr/) Miniconda is already installed. You can search Miniconda module with `search_module Miniconda` and load it with `module load chosen_miniconda_module`.
   * `genotoul` to analyze **your files** with metagWGS **on genologin cluster** with Singularity images `metagWGS.sif`, `eggnog_mapper.sif` and `mosdepth.sif`. 
   * `test_genotoul_workq` to analyze **small test data files** (used in [I. Basic Usage](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#i-basic-usage)) with metagWGS **on genologin cluster** on the **`workq`** queue with Singularity images `metagWGS.sif`, `eggnog_mapper.sif` and `mosdepth.sif`.
   * `test_genotoul_testq` to analyze **small test data files** (used in [I. Basic Usage](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#i-basic-usage)) with metagWGS **on genologin cluster** on the **`testq`** queue with Singularity images `metagWGS.sif`, `eggnog_mapper.sif` and `mosdepth.sif`.
   * `big_test_genotoul` to analyze **big test data files** (used in [Use case documentation page](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/use_case.md)) with metagWGS **on genologin cluster** (on the **`workq`** queue) with Singularity images `metagWGS.sif`, `eggnog_mapper.sif` and `mosdepth.sif`.
   * `test_local` to analyze **small test data files** (used in [I. Basic Usage](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#i-basic-usage)) with metagWGS **on your computer** with Singularity images `metagWGS.sif`, `eggnog_mapper.sif` and `mosdepth.sif`.
   * `debug` to **debug** metagWGS pipeline.

These profiles are associated to different configuration files developped [in this directory](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/tree/master/conf). The `base.config` file available in this directory is the base configuration load in first which is crushed by indications of the profile you use. See [here](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/nfcore/profiles.html) for more explanations.

### 2. Useful options

#### `-resume`

It allows to rerun metagWGS from the lastest process uncorrectly ended or from a process where input or output files have changed.

#### `-with-report`

Generates a `report.html` file describing the use of memory and cpus for each process.

#### `-with-timeline`

Generates a `timeline.html` file describing the duration of each process.

#### `-with-trace`

Generates a `trace.txt` file describing the location of cache directory and metrics for each process.

#### `-with-dag`

Generates a `dag.dot` file, a graph representing the pipeline.

#### `-w working_directory_name`

Allows to choose the name of the cache directory. Default `-w work`.

## IV. metagWGS parameters

The next parameters can be used when you run metagWGS.

**NOTE:** the specific parameters of the pipeline are indicated by `--` in the command line.

### 1. Mandatory parameter: `--reads`

`--reads "<PATH>/*_{R1,R2}.fastq.gz"`: indicate location of `.fastq` or `.fastq.gz` input files. For example, `--reads "<PATH>/*_{R1,R2}.fastq.gz"` run the pipeline with all the `R1.fastq.gz` and `R2.fastq.gz` files available in the indicated `<PATH>`. For a cleaner MultiQC html report at the end of the pipeline, raw data with extensions `_R1` and `_R2` are preferred to those with extensions `_1` and `_2`.

### 2. `--step`

`--step "your_step"`: indicate the step of the pipeline you want to run. The steps available are described in the [`README`](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/tree/master#metagwgs-steps) (`01_clean_qc`, `02_assembly`, `03_filtering`, `04_structural_annot`, `05_alignment`, `06_func_annot`, `07_taxo_affi` and `08_binning`).

**NOTES:**

**i. You can directly indicate the final step that is important to you. For example, if you are interested in binning (and the taxonomic affiliation of bins), just use `--step "08_binning"`. It runs the mandatory previous steps automatically (01_clean_qc to 05_alignment ; except `03_filtering`, see ii).**

**ii. `03_filtering` is automatically skipped for the next steps `04_structural_annot`, `05_alignment`, `06_func_annot`, `07_taxo_affi` and `08_binning`. If you want to filter your assembly before doing one of these steps, you must use `--step "03_filtering,the_step"`, for example `--step "03_filtering,04_structural_annot"`.**

**iii. When you run one of the three steps `06_func_annot`, `07_taxo_affi` or `08_binning` during a first analysis and then another of these steps interests you and you run metagWGS again to get the result of this other step, you have to indicate `--step "the_first_step,the_second_step"`. This will allow you to have a final MultiQC html report that will take into account the metrics of both analyses performed. If the third of these steps interests you and you run again metagWGS for this step, you also have to indicate `--step "the_first_step,the_second_step,the_third,step"` for the same reasons.**

When you want to run a particular step, you just need to specify its name:

   * `01_clean_qc` step: `--step "01_clean_qc"`. This step is automatically done in all others steps.
   If you want to skip this step into the other steps, add parameter `--skip_01_clean_qc` in your command line. Usefull when you have already checked and cleaned your `.fastq` files: you can put in input data (`--reads` parameter) your cleaned `.fastq` files and run directly the `02_assembly` step or other steps.

   * `02_assembly` step: `--step "02_assembly"`. This step is automatically done in all others steps. Assembly is done on reads cleaned with `01_clean_qc` step.

   * `03_filtering` step: `--step "03_filtering"`. **WARNING:** By default, if you want to run one of the next steps (`04_structural_annot`, `05_alignment`, `06_func_annot`, `07_taxo_affi` and `08_binning`), `03_filtering` is **not done**. If you want to do the `03_filtering` step when you run these steps, you must indicate `--step "03_filtering,the_step"` with `the_step` a step in `04_structural_annot`, `05_alignment`, `06_func_annot`, `07_taxo_affi` and `08_binning`.

   * `04_structural_annot` step: `--step "04_structural_annot"`. This step is automatically done in all others steps

   * `05_alignment` step: `--step "05_alignment"`. This step is automatically done in all others steps

   * `06_func_annot` step: `--step "06_func_annot"`.

   * `07_taxo_affi` step: `--step "07_taxo_affi"`.

   * `08_binning` step: `--step "08_binning"`.

Default: `01_clean_qc`.

For each [step](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/README.md#metagwgs-steps), specific parameters are available. You can add them to the command line and run the pipeline. They are described in the next section: [other parameters step by step](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#other-parameters-step-by-step).

### 3. Other parameters step by step

#### **`01_clean_qc` step:**

**NOTE:** this step can be skipped with `--skip_01_clean_qc` parameter.  See **WARNING 1**.

There are 5 substeps in this step each of them with specific parameters:

**1. Remove adapter sequences and low quality reads with cutadapt**

* `--adapter1 "adapter_sequence"`: nucleotidic sequence of read 1 adapter (cutadapt -a option). Default `"AGATCGGAAGAGC"`: [GeT-PlaGe](https://ng6.toulouse.inra.fr/index.php?id=57) Illumina adapters. This adapters depends on the library kit used before sequencing data.

* `--adapter2 "adapter_sequence"`: nucleotidic sequence of read 2 adapter (cutadapt -A option). Default `"AGATCGGAAGAGC"`: [GeT-PlaGe](https://ng6.toulouse.inra.fr/index.php?id=57) Illumina adapters. This adapters depends on the library kit used before sequencing data.

**2. Remove low quality reads with sickle**

* `--skip_sickle`: allows to skip sickle substep. Use: `--skip_sickle`.

* `--quality_type "solexa" or "illumina" or "sanger"`: sickle -t quality type parameter. Default: `"sanger"`.

**3. Remove host reads with bwa, samtools and bedtoools**

* `--skip_removal_host` allows to skip the deletion of host reads. Use: `--skip_removal_host`. See **WARNING 1**.

* `--host_fasta "<PATH>/name_genome.fasta"`: indicate the nucleotide sequence of the host genome. Default: `""`. See **WARNING 1**. Depending on the size of your file, you may need to tweak the memory and cpus settings of the Nextflow process to filter the host reads. If this is the case, create a `nextflow.config` file in our working directory and modify these parameters, such as :
```bash
withName: host_filter {
memory = { 200.GB * task.attempt }
time = '48h'
cpus = 8
}
```
* `--host_bwa_index "<PATH>/name_genome.{amb,ann,bwt,pac,sa}"`: indicate the bwa index files if they are already built. Default: `""` corresponding to the building of bwa index files by metagWGS. See **WARNING 1**.

**WARNING 1:** you need to use `--skip_removal_host` or `--host_fasta` or `--skip_01_clean_qc`. If it is not the case, an error message will occur.

**4. Quality control of raw data and cleaned data with fastQC**

No parameter available for this substep.

**5. Taxonomic classification of reads with kaiju**

* `--kaiju_db_dir "<PATH>/directory"`: if you have already downloaded the kaiju database, indicate its directory. **WARNING:** you will not be able to use kaiju database built with `kaiju-makedb` command line. Default: `--kaiju_db_dir false`. See **WARNING 2**.

* `--kaiju_db "http://kaiju.binf.ku.dk/database/CHOOSEN_DATABASE.tgz"`: allows metagWGS to download the kaiju database of your choice. The list of kaiju databases is available in [kaiju website](http://kaiju.binf.ku.dk/server), in the blue insert on the left side. Default: `--kaiju_db https://kaiju.binf.ku.dk/database/kaiju_db_refseq_2021-02-26.tgz`. See **WARNING 2**.

* `--skip_kaiju`: allows to skip taxonomic affiliation of reads with kaiju. Krona files will not be generated. Use: `--skip_kaiju`. See **WARNING 2**.

**WARNING 2:** you need to use `--kaiju_db_dir` or `--kaiju_db` or `--skip_kaiju`. If it is not the case, an error message will occur.

#### **`02_assembly` step:**

**WARNING 3:** `02_assembly` step depends on `01_clean_qc` step. You need to use the mandatory files of these two steps to run `02_assembly`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS 1 and 2.

* `--assembly ["metaspades" or "megahit"]`: allows to indicate the assembly tool. Default: `metaspades`.

**WARNING 4:** the user has choice between `metaspades` or `megahit` for `--assembly` parameter. The choice can be based on CPUs and memory availability: `metaspades` needs more CPUs and memory than `megahit` but our tests showed that assembly metrics are better for `metaspades` than `megahit`.

* `--metaspades_mem [memory_value]`: memory (in Gb) used by `metaspades` process. Default: `440`.

#### **`03_filtering` step:**

**WARNING 5:** `03_filtering` step depends on `01_clean_qc` and `02_assembly` steps. You need to the use mandatory files of these three steps to run `03_filtering`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS 1, 2, 3 and 4.

**WARNING 6:** this step is not done by default when you launch next steps.

* `--min_contigs_cpm [cutoff_value]`: CPM (Count Per Million) cutoff to filter contigs with low number of reads. [cutoff_value] can be a decimal number (example: `0.5`). Default: `1`.

#### **`04_structural_annot` step:**

No parameters.

**WARNING 7:** `04_structural_annot` step depends on `01_clean_qc`, `02_assembly` and `03_filtering` (if you use it) steps. You need to use the mandatory files of these four steps to run `04_structural_annot`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS from 1 to 6.

**WARNING 8:** if you haven't associated this step with `03_filtering`, calculation time of `04_structural_annot` can be important. Some cluster queues have defined calculation time, you need to adapt the queue you use to your data.
> For example, if you are on [genologin cluster](http://bioinfo.genotoul.fr/) and you haven't done `03_filtering` step, you can create into your working directory a file `nextflow.config` containing:
> ```bash
> withName: prokka {
>  queue = 'unlimitq'
> }
> ```
> This will launch the `Prokka` command line of step `04_structural_annot` on a calculation queue (`unlimitq`) where the job can last more than 4 days (which is not the case for the usual `workq` queue).

#### **`05_alignment` step:**

**WARNING 9:** `05_alignment` step depends on `01_clean_qc`, `02_assembly`, `03_filtering` (if you use it) and `04_structural_annot` steps. You need to use the mandatory files of these five steps to run `05_alignment`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS from 1 to 8.

* `--diamond_bank "<PATH>/bank.dmnd"`: path to diamond bank used to align protein sequence of genes. This bank must be previously built with [diamond makedb](https://github.com/bbuchfink/diamond/wiki). Default `""`.

**WARNING 10:** You need to use a NCBI reference to have functional links in the output file _Quantifications_and_functional_annotations.tsv_ of `06_func_annot` step

#### **`06_func_annot` step:**

**WARNING 11:** `06_func_annot` step depends on `01_clean_qc`, `02_assembly`, `03_filtering` (if you use it), `04_structural_annot` and `05_alignment` steps. You need to use the mandatory files of these six steps to run `06_func_annot`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS from 1 to 9.

* `--percentage_identity [number]`: corresponds to cd-hit-est -c option to indicate sequence percentage identity for clustering genes. Default: `0.95` corresponding to 95% of sequence identity. Use: `number` must be between 0 and 1, and use `.` when you want to use a float.

* `--eggnogmapper_db`: downloads eggNOG-mapper database. If you don't use this parameter, metagWGS doesn't download this database and you must use `--eggnog_mapper_db_dir`. Use: `--eggnogmapper_db`. See **WARNING 6**.

* `--eggnog_mapper_db_dir "<PATH>/database_directory/"`: indicates path to eggNOG-mapper database if you have already dowloaded it. If you run the `06_func_annot` step in different metagenomics projects, downloading the eggNOG-mapper database only once before running metagWGS avoids you to multiply the storage of this database and thus keep free disk space. See **WARNING 6**.

**WARNING 12:** you need to use `--eggnogmapper_db` or `--eggnog_mapper_db_dir`. If it is not the case, an error message will occur.

#### **`07_taxo_affi` step:**

**WARNING 13:** `07_taxo_affi` step depends on `01_clean_qc`, `02_assembly`, `03_filtering` (if you use it), `04_structural_annot` and `05_alignment` steps. You need to use the mandatory files of these six steps to run `07_taxo_affi`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS from 1 to 9.

* `--accession2taxid "FTP_PATH_TO_prot.accession2taxid.gz"`: indicates the FTP adress of the NCBI file `prot.accession2taxid.gz`. Default: `"ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz"`.

* `--taxdump "FTP_PATH_TO_taxdump.tar.gz"`: indicates the FTP adress of the NCBI file `taxdump.tar.gz`. Default `"ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"`.

* `--taxonomy_dir "<PATH>/directory": if you have already downloaded the accession2taxid and taxdump databases, indicate their parent directory. Default: `--taxonomy_dir false`.`

#### **`08_binning` step:**

**WARNING 14:** `08_binning` step depends on `01_clean_qc`, `02_assembly`, `03_filtering` (if you use it), `04_structural_annot` and `05_alignment` steps. You need to use the mandatory files of these six steps to run `08_binning`. See [II. Input files](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#ii-input-files) and WARNINGS from 1 to 9.

* `--min_contig_size [cutoff_length]`: contig length cutoff to filter contigs before binning. Must be greater than `1500`. Default: `1500`.

* `--busco_reference "<PATH>/file_db"`: path to BUSCO database. Default: `"https://busco-archive.ezlab.org/v3/datasets/bacteria_odb9.tar.gz"`. **WARNING 15:** We use BUSCO v3 from the `metagWGS.sif` Singularity container. Be careful not to use the BUSCO reference of other BUSCO versions.

* `--cat_db "<PATH>/CAT_prepare_20190108.tar.gz"`: path to CAT/BAT database. Default: `false`. **WARNING 16:** you need to download this database before running metagWGS `08_binning` step. Download it with: `wget tbb.bio.uu.nl/bastiaan/CAT_prepare/CAT_prepare_20210107.tar.gz`.

#### Others parameters

* `--multiqc_config "<PATH>/multiqc.yaml"`: if you want to change the configuration of multiqc report. Default: `"$baseDir/assets/multiqc_config.yaml"`.

* `--outdir "dir_name"`: change the name of output directory. Default `"results"`.

* `--help`: print metagWGS help. Default: `false`. Use: `--help`.

## V. Description of output files

See the description of output files in [this part](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/output.md) of the documentation or [there](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/functional_tests/README.md#iii-output).

## VI. Analyze big test dataset with metagWGS in genologin cluster

> If you have an account in [genologin cluster](http://bioinfo.genotoul.fr/) and you would like to familiarize yourself with metagWGS, see the tutorial available in the [use case documentation page](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/use_case.md). It allows the analysis of big test datasets with metagWGS.

**WARNING 17:** the test dataset in `small_metagwgs-test-datasets` used in [I. Basic Usage](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md#i-basic-usage) is a small test dataset which does not allow to test all steps (`08_binning` doesn't work with this dataset).
