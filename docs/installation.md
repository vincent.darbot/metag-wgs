# metagWGS: installation procedure

## I. Install Nextflow

metagWGS is a [Nextflow](https://www.nextflow.io/) pipeline, you must install Nextflow to run it.

Nextflow runs on most POSIX systems (Linux, Mac OSX etc). 

It can be installed by following the instructions [here](https://www.nextflow.io/docs/latest/getstarted.html#installation). 

**WARNING:** change your $PATH variable to run Nextflow in any directory.

For any issue during Nextflow installation and use, you can see [this tutorial](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/nextflow/usage.html) and [this documentation](https://www.nextflow.io/docs/latest/index.html).

**NOTE: if you are on [genologin cluster](http://bioinfo.genotoul.fr/) you don't need to install Nextflow**

You only need to know Nextflow version installed on genologin.
* Run `search_module Nextflow` to find Nextflow available versions.
* Keep in mind the last version of Nextflow installed (>=v20).

   **WARNING:** you must use Nextflow versions available [here](http://bioinfo.genotoul.fr/index.php/how-to-use/?software=How_to_use_SLURM_Nextflow). Don't use `bioinfo/nfcore-Nextflow` versions of Nextflow.

## II. Install metagWGS

**In the directory you want tu run the workflow**, launch:

```bash
git clone https://forgemia.inra.fr/genotoul-bioinfo/metagwgs.git
```

A directory called `metagwgs` containing all source files of the pipeline have been downloaded.

## III. Install Singularity

metagWGS needs three [Singularity](https://sylabs.io/docs/) containers to run: Singularity version 3 or above must be installed.

See [here](https://sylabs.io/guides/3.7/user-guide/quick_start.html#quick-installation-steps) how to install Singularity >=v3.

**NOTE: you are on [genologin cluster](http://bioinfo.genotoul.fr/)**
* Run `search_module Singularity` to find Singularity available versions.
* Run `module load Singularity_module` of the last version (`Singularity_module`) of Singularity installed (>=v3).
* **WARNING:** for any issue during Singularity use on genologin cluster, you can see [this tutorial](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/singularity/SINGULARITY.html).

## IV. Download or build Singularity containers

You can directly download the three Singularity containers (`Solution 1`, recommended) or build them (`Solution 2`).

### Solution 1 (recommended): download the three containers

**In the directory you want tu run the workflow**, where you have the directory `metagwgs` with metagWGS source files, run these command lines:

```bash
cd metagwgs/env/
singularity pull eggnog_mapper.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/eggnog_mapper:latest
singularity pull metagwgs.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/metagwgs:latest
singularity pull mosdepth.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/mosdepth:latest
```

Three files (`metagwgs.sif`, `eggnog_mapper.sif` and `mosdepth.sif`) must have been downloaded.

### Solution 2: build the three containers.

**In the directory you want tu run the workflow**, where you have downloaded metagWGS source files, go to `metagwgs/env/` directory, and follow [these explanations](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/wikis/Singularity%20container) to build the three containers. You need three files by container to build them. These files are into the `metagwgs/env/` folder and you can read them here:

* metagwgs.sif container
   * [metagWGS recipe file](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/Singularity_recipe_metagWGS)
   * [metagWGS.yml](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/metagWGS.yml)
* eggnog_mapper.sif container
   * [eggnog_mapper recipe file](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/Singularity_recipe_eggnog_mapper)
   * [eggnog_mapper.yml](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/eggnog_mapper.yml)
* mosdepth.sif container
   * [mosdepth recipe file](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/Singularity_recipe_mosdepth)
   * [mosdepth.yml](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/mosdepth.yml)

At the end of the build, three files (`metagwgs.sif`, `eggnog_mapper.sif` and `mosdepth.sif`) must have been generated.

**WARNING:** to ensure Nextflow can find the _.sif_ files, we encourage you to change the _nextflow.config_ file in metagWGS to contain these lines:
```
process {
    container = '<PATH>/metagwgs.sif'
    withLabel: eggnog {
        container = '<PATH>/eggnog_mapper.sif'
    }
    withLabel: mosdepth {
        container = '<PATH>/mosdepth.sif'
    }
}
```
Where \<PATH\> leads to the directory where the singularity images are built/downloaded.

## V. Use metagWGS

See the [Usage](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/docs/usage.md) documentation to know how to run metagWGS.
