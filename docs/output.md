# metagWGS: Output

## Introduction

This document describes the output files produced by metagWGS.

## Description of output files and directories

### The pipeline will create the following folders in your working directory:

| Directory/        | Description                                    |
| ------------- | --------------------------------------- |
| `work/`          | Directory containing the Nextflow working files. Directory name can be changed if you use -w option of Nextflow.     |
| `results/`       | Directory containing metagWGS output files. Directory name can be changed if you use --outdir parameter of metagWGS. |

### Description of output files in `results/` directory:

The `results/` directory contains a sub-directory for each step launched:

#### **01_clean_qc/01_1_cleaned_reads/**

| File or directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `cleaned_SAMPLE_NAME_R{1,2}.fastq.gz`        | Cleaned reads after `01_clean_qc` step. There are one R1 and one R2 file for each sample. |
| `logs/`    | Contains cutadapt (`SAMPLE_NAME_cutadapt.log`) and sickle (`SAMPLE_NAME_sickle.log`) log files for each sample. Only if you remove host reads, in `SAMPLE_NAME_cleaned_R{1,2}.nb_bases` you have the number of nucleotides into each cleaned R1 and R2 files of each sample. Only if you remove host reads, you also have a samtools flagstat file for each sample before removing host reads (`SAMPLE_NAME.no_filter.flagstat`) and into the directory `host_filter_flagstat/` there are the samtools flagstat files (`SAMPLE_NAME.host_filter.flagstat`) after removing of host reads.          |

#### **01_clean_qc/01_2_qc/**

| Directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `fastqc_raw/SAMPLE_NAME/`       | Contains the results of fastQC on raw data: two files (`.html` and `.zip`) for R1 and two files for R2 (`.html` and `.zip`). |
| `fastqc_cleaned/cleaned_SAMPLE_NAME/`   | Same as `01_2_qc/fastqc_raw/` but for cleaned data. |

#### **01_clean_qc/01_3_taxonomic_affiliation_reads/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.krona.html`        | Krona file representing number of reads by taxonomical level. One krona is generated for each sample. |
| `taxo_affi_reads_[taxo_level].txt`        | One file for each taxonomy level (phylum, order, class, family, genus, species). The first column corresponds to the name of the taxon, the second to the taxon id and then for each file there are 2 columns: the percentage of reads affiliated to this taxon and the number of reads affiliated to this taxon. |

#### **02_assembly/**

**NOTE:** in this directory you have either the results of assembly with `metaspades` or `megahit` but not both. You have chosen your assembly tool with `--assembly` parameter.

| File or directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `metaspades/SAMPLE_NAME.log` | metaspades logs. Only if `--assembly "metaspades"` is used.|
| `metaspades/SAMPLE_NAME.params.txt` | metaspades command line parameters used for the analysis. Only if `--assembly "metaspades"` is used.|
| `metaspades/SAMPLE_NAME.contigs.fa` | metaspades assembly: nucleotide sequence of contigs. Only if `--assembly "metaspades"` is used.|
| `megahit/SAMPLE_NAME.log` | megahit logs. Only if `--assembly "megahit"` is used.|
| `megahit/SAMPLE_NAME.params.txt` | megahit options used for the analysis. Only if `--assembly "megahit"` is used.|
| `megahit/SAMPLE_NAME.contigs.fa` | megahit assembly: nucleotide sequence of contigs. Only if `--assembly "megahit"` is used.|
| `SAMPLE_NAME_all_contigs_QC/` | Contains metaQUAST quality control files of contigs. |
| `SAMPLE_NAME_R{1,2}_dedup.fastq.gz` | Deduplicated reads (R1 and R2 files) for SAMPLE_NAME sample. |
| `logs/` | Contains `SAMPLE_NAME.count_reads_on_contigs.flagstat`, `SAMPLE_NAME.count_reads_on_contigs.idxstats` and `SAMPLE_NAME_dedup_R{1,2}.nb_bases` files for each sample, generated after deduplication of reads. `SAMPLE_NAME.count_reads_on_contigs.flagstat` and `SAMPLE_NAME.count_reads_on_contigs.idxstats` are respectively the results of samtools flagstat (see informations [here](http://www.htslib.org/doc/samtools-flagstat.html)) and samtools idxstats (see description [here](http://www.htslib.org/doc/samtools-idxstats.html)), `SAMPLE_NAME_R{1,2}.nb_bases` corresponds to the number of nucleotides into deduplicated R1 and R2 files. |

#### **03_filtering/**

| File or directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME_select_contigs_cpm[percent_identity].fasta` | Nucleotide sequence of contigs selected after filtering step with a percentage of identity of [percent_identity]. |
| `SAMPLE_NAME_discard_contigs_cpm[percent_identity].fasta` | Nucleotide sequence of contigs discarded after filtering step with a percentage of identity of [percent_identity]. |
| `SAMPLE_NAME_select_contigs_QC/` | Contains metaQUAST quality control files of filtered contigs. |

#### **04_structural_annot/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.annotated.faa` | Protein sequence of structural annotated genes. |
| `SAMPLE_NAME.annotated.ffn` | Nucleotide sequence of structural annotated genes. |
| `SAMPLE_NAME.annotated.gff` | Coordinates of structural annotated genes into contigs. |
| `SAMPLE_NAME.annotated.fna` | Nucleotide sequence of contigs used by Prokka for the annotation of genes. In the used version of Prokka, it removes short contigs (<200bp). **WARNING:** these contigs are used in the following analysis. |

**WARNING: from this step, the gene names follow this nomenclature: SAMPLE_NAME_cCONTIG_ID.Prot_PROT_ID. Contig names follow the same nomenclature: SAMPLE_NAME_cCONTIG_ID.**

#### **05_alignment/05_1_reads_alignment_on_contigs/**

**WARNING:** contains a directory by sample. Each directory contains the following files:

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.sort.bam` | Alignment of reads on contigs (.bam file). |
| `SAMPLE_NAME.sort.bam.bai` | Index of .bam file. |
| `SAMPLE_NAME.sort.bam.idxstats` | Samtools idxstats file. See description [here](http://www.htslib.org/doc/samtools-idxstats.html). |

#### **05_alignment/05_2_database_alignment/**

| File or directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `head.m8` | Header of all .m8 files in all the next folders. |
| `SAMPLE_NAME/SAMPLE_NAME_aln_diamond.m8` | Diamond results file. See the head.m8 file to see each column title. |

#### **06_func_annot/06_1_clustering/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.cd-hit-est.[cd-hit percentage identity].fasta` | Nucleotide sequences of representatives genes ("intra-sample clusters") generated by cd-hit-est with [cd-hit percentage identity] percentage identity. |
| `SAMPLE_NAME.cd-hit-est.[cd-hit percentage identity].fasta.clstr` | Text file of list of intra-sample clusters generated by cd-hit-est with [cd-hit percentage identity] percentage identity. |
| `SAMPLE_NAME.cd-hit-est.[cd-hit percentage identity].table_cluster_contigs.txt` | Correspondance table of intra-sample clusters and initial genes. One line = one correspondance between an intra-sample cluster (first column) and an initial gene (second column). |
| `All-cd-hit-est.[cd-hit percentage identity].fasta` | Nucleotide sequences of global representatives genes ("inter-sample clusters") generated by cd-hit-est with [cd-hit percentage identity] percentage identity. |
| `All-cd-hit-est.[cd-hit percentage identity].fasta.clstr` | Text file of list of inter-sample clusters generated by cd-hit-est with [cd-hit percentage identity] percentage identity. |
| `table_clstr.txt` | Correspondance table of inter-sample clusters and intra-sample clusters. One line = one correspondance between an inter-sample cluster (first column) and an intra-sample cluster (second column). |

#### **06_func_annot/06_2_quantification/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.featureCounts.tsv.summary` | featureCounts statistics by sample. |
| `SAMPLE_NAME.featureCounts.stdout` | featureCounts log file by sample. |
| `SAMPLE_NAME.featureCounts.tsv` | featureCounts output file by sample.|
| `Correspondence_global_clstr_contigs.txt` | Correspondance table of inter-sample clusters and initial genes. One line = one correspondance between an inter-sample cluster (first column) and an initial gene (second column). |
| `Clusters_Count_table_all_samples.txt` | Abundance table of reads. Each cell contains the sum of aligned reads on each initial gene of each inter-sample cluster for each sample (inter_sample clusters in rows and samples in colums).|

#### **06_func_annot/06_3_functional_annotation/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME_diamond_one2one.emapper.seed_orthologs` | eggNOG-mapper intermediate file containing seed match into eggNOG database. |
| `SAMPLE_NAME_diamond_one2one.emapper.annotations` | eggNOG-mapper final file containing functional annotations for genes with a match into eggNOG database. |
| `SAMPLE_NAME.best_hit` | Diamond best hits results for each gene. For a gene, best hits are diamond hits with the maximum bitScore for this gene. |
| `Quantifications_and_functional_annotations.tsv` | Table where a row corresponds to an inter-sample cluster. Columns corresponds to quantification of the sum of aligned reads on all gens of each inter-sample cluster (columns `*featureCounts.tsv`), sum of abundance in all samples (column `sum`), eggNOG-mapper results (from `seed_eggNOG_ortholog` to `PFAMs` column) and diamond best hits results (last two columns `diamond_db_id`and `diamond_db_description`). |
| `GOs_abundance.tsv` | Quantification table storing for each GO term (rows) the sum of aligned reads into all genes having this functional annotation for each sample (columns). |
| `KEGG_ko_abundance.tsv` | Quantification table storing for each KEGG_ko (rows) the sum of aligned reads into all genes having this functional annotation for each sample (columns). |
| `KEGG_Pathway_abundance.tsv` | Quantification table storing for each KEGG_Pathway (rows) the sum of aligned reads into all genes having this functional annotation for each sample (columns). |
| `KEGG_Module_abundance.tsv` | Quantification table storing for each KEGG_Module (rows) the sum of aligned reads into all genes having this functional annotation for each sample (columns). |
| `PFAM_abundance.tsv` | Quantification table storing for each PFAM (rows) the sum of aligned reads into all genes having this functional annotation for each sample (columns). |

#### **07_taxo_affi/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME/SAMPLE_NAME.pergene.tsv` | Taxonomic affiliation of genes. One line corresponds to a gene (1st column), its corresponding taxon id (2nd column), its corresponding lineage (3rd column) and the tax ids of each level of this lineage (4th column). |
| `SAMPLE_NAME/SAMPLE_NAME.warn.tsv` | List of genes with a hit without corresponding taxonomic affiliation. Each line corresponds to a gene (1st column), the reason why the gene is in this list (2nd column) and match ids into the database used during `05_alignment/05_2_database_alignment/` (3rd column). |
| `SAMPLE_NAME/SAMPLE_NAME.percontig.tsv` | Taxonomic affiliation of contigs. One line corresponds to a contig (1st column), its corresponding taxon id (2nd column), its corresponding lineage (3rd column) and the tax ids of each level of this lineage (4th column). |
| `SAMPLE_NAME/SAMPLE_NAME_quantif_percontig.tsv` | Quantification table of reads aligned on contigs affiliated to each lineage of the first column. One line = one taxonomic affiliation (1st column, `lineage_by_level`), the corresponding taxon id (2nd column, `consensus_tax_id`), the tax ids of each level of this taxonomic affiliation (3rd column, `tax_id_by_level`), the name of contigs affiliated to this lineage (4th column, `name_contigs`), the number of contigs affiliated to this lineage (5th column, `nb_contigs`), the sum of the number of reads aligned to these contigs (6th column, `nb_reads`) and the mean depth of these contigs (6th column, `depth`). |
| `SAMPLE_NAME/SAMPLE_NAME_quantif_percontig_by_[taxonomic_level].tsv` | One file by taxonomic level (superkingdom, phylum, order, class, family, genus, species) for the sample `SAMPLE_NAME`. Quantification table of reads aligned on contigs affiliated to each lineage of the corresponding [taxonomic level]. One line = one taxonomic affiliation at this [taxonomic level] with is taxon id (1st column, `tax_id_by_level`), its lineage (2nd column, `lineage_by_level`), the name of contigs affiliated to this lineage (3rd column, `name_contigs`), the number of contigs affiliated to this lineage (4th column, `nb_contigs`), the sum of the number of reads aligned to these contigs (5th column, `nb_reads`) and the mean depth of these contigs (6th column, `depth`). |
| `SAMPLE_NAME/graphs/SAMPLE_NAME_aln_diamond.m8_contig_taxonomy_level.pdf` | Figure representing the number of contigs (y-axis) affiliated to each taxonomy levels (x-axis). |
| `SAMPLE_NAME/graphs/SAMPLE_NAME_aln_diamond.m8_prot_taxonomy_level.pdf` | Figure representing the number of proteins (y-axis) affiliated to each taxonomy levels (x-axis). |
| `SAMPLE_NAME/graphs/SAMPLE_NAME_aln_diamond.m8_nb_prot_annotated_and_assigned.pdf` | Figure representing the number of proteins (y-axis) in our contigs (`Total` bar), the number of proteins with a match into the database (`Annotated` bar) and the number of proteins with a match into the database which is found into the taxonomy (`Assigned` bar) (x-axis). |
| `quantification_by_contig_lineage_all.tsv` | Quantification table of reads aligned on contigs affiliated to each lineage. One line = one taxonomic affiliation with its lineage (1st column, `lineage_by_level`), the taxon id at each level of this lineage (2nd column, `tax_id_by_level`), and then all next 3-columns blocks correspond to one sample. Each 3-column block corresponds to the name of contigs affiliated to this lineage (1st column, `name_contigs_SAMPLE_NAME_quantif_percontig`), the number of contigs affiliated to this lineage (2nd column, `nb_contigs_SAMPLE_NAME_quantif_percontig`), the sum of the number of reads aligned to these contigs (3rd column, `nb_reads_SAMPLE_NAME_quantif_percontig`) and the mean depth of these contigs (4th column, `depth_SAMPLE_NAME_quantif_percontig`). |
| `quantification_by_contig_lineage_[taxonomic_level].tsv` | One file by taxonomic level (superkingdom, phylum, order, class, family, genus, species). Quantification table of reads aligned on contigs affiliated to each lineage of the corresponding [taxonomic level]. One line = one taxonomic affiliation at this [taxonomic level] with its taxon id (1st column, `tax_id_by_level`), its lineage (2nd column, `lineage_by_level`), and then all next 3-columns blocks correspond to one sample. Each 3-column block corresponds to the name of contigs affiliated to this lineage (1st column, `name_contigs_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`), the number of contigs affiliated to this lineage (2nd column, `nb_contigs_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`) and the sum of the number of reads aligned to these contigs (3rd column, `nb_reads_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`) and the mean depth of these contigs (4th column, `depth_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`). |

#### **08_binning/08_1_binning/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `MetaBAT2/SAMPLE_NAME.id_bin.fa` | Nucleotide sequence of contigs group together into the bin `id_bin` in the sample `SAMPLE_NAME`. |

#### **08_binning/08_2_QC/**

| File or directory/      | Description                             |
| ----------------------- | --------------------------------------- |
| `QUAST/` | All metaQUAST QC about bins. |
| `BUSCO/` | All BUSCO QC about bins. |
| `busco_summary.txt` | Summary of BUSCO results. |
| `quast_summary.tsv` | Summary of metaQUAST results. |
| `quast_and_busco_summary.tsv` | Summary of metaQUAST and BUSCO results. |
| `SAMPLE_NAME-busco_figure.png` | BUSCO quality assessment figure. |

#### **08_binning/08_3_taxonomy/**

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.bin2classification.names.txt` | BAT taxonomic classification of bins. One line = one bin (1st column, `bin`). See [this file](https://github.com/dutilh/CAT#interpreting-the-output-files) for explanations. |
| `SAMPLE_NAME.ORF2LCA.names.txt` | BAT taxonomic classification of ORF. One line = one ORF (1st column, `ORF`), its corresponding bin (2nd column, `bin`), "the bit-score the top-hit bit-score that is assigned to the ORF for voting" (3rd column, `lineage bit-score`) and the lineage name (4th column, `full lineage names`). See [this file](https://github.com/dutilh/CAT#interpreting-the-output-files) for explanations. |
| `raw/SAMPLE_NAME.log` | BAT log file. |
| `raw/SAMPLE_NAME.bin2classification.txt` | Same as `08_3_taxonomy/SAMPLE_NAME.bin2classification.names.txt` without last column. |
| `raw/SAMPLE_NAME.ORF2LCA.txt` | Same as `08_3_taxonomy/SAMPLE_NAME.ORF2LCA.names.txt` without last column. |
| `raw/SAMPLE_NAME.concatenated.predicted_proteins.gff` | Annotation of proteins into each contig of each bin by BAT. Each line corresponds to `SAMPLE_NAME.id_bin.fa_SAMPLE_NAME_cCONTIG_id`. |
| `raw/SAMPLE_NAME.predicted_proteins.faa` | Protein sequence of proteins predicted into each contig of each bin by BAT. Each line corresponds to `SAMPLE_NAME.id_bin.fa_SAMPLE_NAME_cCONTIG_id`. |

#### **MultiQC/**

| File                    | Description                             |
| ----------------------- | --------------------------------------- |
| `multiqc_report.html` | MultiQC report file containing graphs and a summary of analysis done by metagWGS. |

#### **pipeline_info/**

| File                    | Description                             |
| ----------------------- | --------------------------------------- |
| `software_versions.csv` | Indicates the versions of the tools used in the pipeline.     |

### Description of other files in your working directory (not in `results` directory):

| File          | Description            |
| ------------- | ---------------------- |
| `.nextflow_log` | Log file from Nextflow. |

### Other files can be added to the working directory (not in `results` directory) if you use Nextflow specific options:

| Option        | File            | Description            |
| ------------- | --------------- | ---------------------- |
| `-with-trace` | `trace.txt` | Execution tracing file of the pipeline, containing informations as location of cache directories and memory and cpus statistics for each job. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#trace-report). |
| `-with-report ` | `report.html` | Execution metrics of workflow execution. Contains metadata with shell commands launched by each process. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#execution-report). |
| `-with-timeline` | `timeline.html` | Time metrics of workflow execution. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#timeline-report). |
| `-with-dag` | `dag.dot` | Modelisation of the pipeline by a Direct Acyclic Graph. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#dag-visualisation). |
