# Functional tests: Usage

## I. Pre-requisites

1. Install metagwgs as described here: [installation doc](../docs/installation.md)
2. Get datasets: two datasets are currently available for these functional tests at `https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets.git`

    Replace "\<dataset\>" with either "small" or "mag":
    ```
    git clone --branch <dataset> git@forgemia.inra.fr:genotoul-bioinfo/metagwgs-test-datasets.git

    or

    wget https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets/-/archive/<dataset>/metagwgs-test-datasets-<dataset>.tar.gz
    ```
3. Get data banks: download [this archive](http://genoweb.toulouse.inra.fr/~choede/FT_banks_2021-10-19.tar.gz) and decompress its contents in any folder. This archive contains data banks for:
    - **Kaiju** (_kaijudb_refseq_2020-05-25_)
    - **Diamond** (_refseq_bacteria_2021-05-20_)
    - **NCBI Taxonomy** (_taxonomy_2021-08-23_)
    - **Eggnog Mapper** (_eggnog-mapper-2.0.4-rf1_)


    > Use those banks to reproduce the outputs of functional tests.

## II. Run functional tests

Each step of metagwgs produces a series of files. We want to be able to determine if the modifications we perform on metagwgs have an impact on any of these files (presence, contents, format, ...). You will find more info about how the files are tested at the end of this page.

To launch functional tests, you need to be located at the root of the folder where you want to perform the tests. There are two ways to launch functionnal tests (testing all steps up to **07_taxo_affi**):
- by providing the results folder of a pipeline already exectuted
    ```
    cd test_folder
    python metagwgs/functional_tests/main.py -step 07_taxo_affi -exp_dir metagwgs-test-datasets-small/output -obs_dir ./results
    ```
- by providing a script which will launch the nextflow pipeline [see example](./launch_example.sh) (this example is designed for the "small" dataset with --min_contigs_cpm>1000, using slurm)
    ```
    mkdir test_folder
    cd test_folder
    cp metagwgs/functional_tests/launch_example.sh ./
    python metagwgs/functional_tests/main.py -step 07_taxo_affi -exp_dir metagwgs-test-datasets-small/output -obs_dir ./results --script launch_example.sh
    ```

>**NOTE: more information on the command used to produce each dataset in [small](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets/-/tree/small) and [mag](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets/-/tree/mag) READMEs**

## III. Output

A `ft_[STEP].log` file is created for each step of metagwgs. It contains information about each test performed on given files.

Exemple with `ft_01_clean_qc.log`:

```
Expected directory: /work/pmartin2/metaG/test_expected_logs/01_clean_qc
vs
Observed directory: /work/pmartin2/metaG/refac_09_13.2/results/01_clean_qc

------------------------------------------------------------------------------

File:           01_1_cleaned_reads/cleaned_c_R1.fastq.gz
Test method:    zdiff
Test result:    Passed

------------------------------------------------------------------------------

File:           01_1_cleaned_reads/cleaned_c_R2.fastq.gz
Test method:    zdiff
Test result:    Passed


...


=========================================
-----------------------------------------

Testing the 01_clean_qc step of metagWGS:

Total:      36
Passed:     36 (100.0%)
Missed:     0 (0.0%)
Not tested: 0

-----------------------------------------
=========================================
```

If a test resulted in 'Failed' instead of 'Passed', the stdout is printed in log.

Sometimes, files are not tested because present in _exp_dir_ but not in _obs_dir_. Then a log `ft_[STEP].not_tested` is created containing names of missing files. In **02_assembly**, there are two possible assembly programs that can be used: _metaspades_ and _megahit_, resulting in this `.not_tested log` file. Not tested files are not counted in missed count.


### Test methods

5 simple test methods are used:


- **diff**: simple bash difference between two files
    
    `diff exp_path obs_path`

- **zdiff**: simple bash difference between two gzipped files

    `zdiff exp_path obs_path`

- **no_header_diff**: remove the headers of .annotations and .seed_orthologs files

    `diff <(grep -w "^?#" exp_path) <(grep -w "^?#" obs_path)`

- **cut_diff**: exception for cutadapt.log file

    `diff <(tail -n+6 exp_path) <(tail -n+6 obs_path)`

- **not_empty**: in python, check if file is empty

    `test = path.getsize(obs_path) > 0`
