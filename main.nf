#!/usr/bin/env nextflow
/*
========================================================================================
                         metagWGS
========================================================================================
 metagWGS Analysis Pipeline.
 #### Homepage / Documentation
 https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/
----------------------------------------------------------------------------------------
*/


/*
 * Define helpMessage
 */

 def helpMessage() {

   log.info"""
   Usage:

     The typical command for running the pipeline is as follows:
       nextflow run -profile standard main.nf --reads '*_{R1,R2}.fastq.gz' --skip_removal_host --skip_kaiju 

     Mandatory arguments:
       --reads [path]                Path to input data (must be surrounded with quotes).

     Options:
     
       --step                        Choose step(s) into: "01_clean_qc", "02_assembly", "03_filtering",
                                     "04_structural_annot", "05_alignment", "06_func_annot", "07_taxo_affi", "08_binning".
                                     i. You can directly indicate the final step that is important to you. For example,
                                     if you are interested in binning (and the taxonomic affiliation of bins), just use --step "08_binning".
                                     It runs the previous steps automatically (except "03_filtering", see ii).
                                     ii. "03_filtering" is automatically skipped for the next steps
                                     "04_structural_annot", "05_alignment", "06_func_annot", "07_taxo_affi" and "08_binning".
                                     If you want to filter your assembly before doing one of these steps, you must use --step "03_filtering,the_step",
                                     for example --step "03_filtering,04_structural_annot".
                                     iii. When you run one of the three steps "06_func_annot", "07_taxo_affi" or "08_binning" during a first analysis
                                     and then another of these steps interests you and you run metagWGS again to get the result of this other step,
                                     you have to indicate --step "the_first_step,the_second_step". This will allow you to have a final MultiQC html report
                                     that will take into account the metrics of both analyses performed. If the third of these steps interests you and you run again
                                     metagWGS for this step, you also have to indicate --step "the_first_step,the_second_step,the_third,step" for the same reasons.
     01_clean_qc options:
       --skip_01_clean_qc            Skip 01_clean_qc step.
       --adapter1                    Sequence of adapter 1. Default: Illumina TruSeq adapter.
       --adapter2                    Sequence of adapter 2. Default: Illumina TruSeq adapter.
       --skip_sickle                 Skip sickle process.
       --quality_type                Type of quality values for sickle (solexa (CASAVA < 1.3), illumina (CASAVA 1.3 to 1.7), sanger (which is CASAVA >= 1.8)). Default: 'sanger'.
       --skip_removal_host           Skip filter host reads process.
       --host_fasta                  Full path to fasta of host genome ("PATH/name_genome.fasta").
       --host_bwa_index              Full path to directory containing BWA index including base name i.e ("PATH/name_genome.{amb,ann,bwt,pac,sa}").
       You need to use --skip_removal_host or --host_fasta or --skip_01_clean_qc. If it is not the case, an error message will occur.
       --skip_kaiju                  Skip taxonomic affiliation of reads with kaiju.
       --kaiju_db_dir                Directory with kaiju database already built ("PATH/directory").
       --kaiju_db                    Indicate kaiju database you want to build. Default: "https://kaiju.binf.ku.dk/database/kaiju_db_refseq_2021-02-26.tgz".
       You need to use --kaiju_db_dir or --skip_kaiju. If it is not the case, an error message will occur.
     
     02_assembly options:
       --assembly                    Indicate the assembly tool ["metaspades" or "megahit"]. Default: "metaspades".
       --metaspades_mem [mem_value]  Memory (in G) used by metaspades process. Default: 440.
       
     03_filtering options:
       --min_contigs_cpm [cutoff]    CPM cutoff (Count Per Million) to filter contigs with low number of reads. Default: 1.
       
     05_alignment options:
       --diamond_bank                Path to diamond bank used to align protein sequence of genes: "PATH/bank.dmnd".
                                     This bank must be previously built with diamond makedb.
       
     06_func_annot options:
       --percentage_identity [nb]    Sequence identity threshold. Default: 0.95 corresponding to 95%. Use a number between 0 and 1.
       --eggnogmapper_db             Use: --eggnogmapper_db if you want that metagwgs build the database. Default false: metagWGS didn't build this database.
       --eggnog_mapper_db_dir        Path to eggnog-mapper database "PATH/database_directory/" if it is already built. Default: false.
       You need to use --eggnogmapper_db or --eggnog_mapper_db_dir. If it is not the case, an error message will occur.
       
     07_taxo_affi options:
       --accession2taxid             FTP adress of file prot.accession2taxid.gz. Default: "ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz".
       --taxdump                     FTP adress of file taxdump.tar.gz. Default: "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz".
       --taxonomy_dir                Directory if taxdump and accession2taxid already downloaded ("PATH/directory").

     08_binning options:
       --min_contig_size [cutoff]    Contig length cutoff to filter contigs before binning. Must be greater than 1500. Default: 1500.
       --busco_reference             BUSCO v3 database. Default: "https://busco-archive.ezlab.org/v3/datasets/bacteria_odb9.tar.gz".
       --cat_db                      CAT/BAT database "PATH/CAT_prepare_20190108.tar.gz". Default: false. Must be previously downloaded.

     Other options:
       --outdir                      The output directory where the results will be saved: "dir_name". Default "results".
       --help                        Show this message and exit.
       --multiqc_config              If you want to change the configuration of multiqc report: "PATH/multiqc.yaml". Default "$baseDir/assets/multiqc_config.yaml".

       -profile                      Configuration profile to use.
                                     Available: singularity (use of Singularity containers), conda (use of conda environments), genotoul (use of metagWGS on genologin cluster),
                                     different test profiles (test_genotoul_workq, test_genotoul_testq, big_test_genotoul, test_local) and debug profile.
   """.stripIndent()
 }

 /*
  * SET UP CONFIGURATION VARIABLES.
  */

// Show help message.

if (params.help){
  helpMessage()
  exit 0
}

// Define list of available steps.

def defineStepList() {
  return [
    '01_clean_qc',
    '02_assembly',
    '03_filtering',
    '04_structural_annot',
    '05_alignment',
    '06_func_annot',
    '07_taxo_affi',
    '08_binning'
  ]
}

// Check step existence.

def checkParameterExistence(list_it, list) {
  nb_false_step = 0
  for(it in list_it) {
    if (!list.contains(it)) {
      log.warn "Unknown parameter: ${it}"
      nb_false_step = nb_false_step + 1
    }
  }
  if(nb_false_step > 0) {return false}
  else {return true}
}

// Check number of steps.

// Set up parameters.

step = params.step.split(",")
stepList = defineStepList()
if (!checkParameterExistence(step, stepList)) exit 1, "Unknown step(s) upon ${step}, see --help for more information"

if (!['metaspades','megahit'].contains(params.assembly)){
  exit 1, "Invalid aligner option: ${params.assembly}. Valid options: 'metaspades', 'megahit'"
}

if (!["solexa","illumina","sanger"].contains(params.quality_type)){
  exit 1, "Invalid quality_type option: ${params.quality_type}. Valid options:'solexa','illumina','sanger'"
}

/*
 * Create channels for adapters, qualityType, alignment genome, mode value.
 */

adapter1_ch = Channel.value(params.adapter1)
adapter2_ch = Channel.value(params.adapter2)

if(!params.skip_busco) {
  Channel
    .fromPath( "${params.busco_reference}", checkIfExists: true )
    .set { file_busco_db }
}
else {
  file_busco_db = Channel.from()
}

if(params.cat_db) {
  Channel
    .fromPath( "${params.cat_db}", checkIfExists: true )
    .set { file_cat_db }
}
else {
  file_cat_db = Channel.from()
}

if(params.host_fasta) {
  bwa_fasta_ch = Channel.value(file(params.host_fasta))
}
else {
  bwa_fasta_ch = Channel.from()
}

diamond_bank_ch = Channel.value(params.diamond_bank)
accession2taxid_ch = Channel.value(params.accession2taxid)
taxdump_ch = Channel.value(params.taxdump)
percentage_identity_ch = Channel.value(params.percentage_identity)
min_contigs_cpm_ch = Channel.value(params.min_contigs_cpm)
metaspades_mem_ch = Channel.value(params.metaspades_mem)

multiqc_config_ch = file(params.multiqc_config, checkIfExists: true)
/*
 * Create channels for input read files.
 */
Channel
  .fromFilePairs( params.reads, size: params.single_end ? 1 : 2, flat: true )
  .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}\nNB: Path needs to be enclosed in quotes!\nNB: Path requires at least one * wildcard!\nIf this is single-end data, please specify --singleEnd on the command line." }
  .into { raw_reads_fastqc; raw_reads_cutadapt; raw_reads_assembly_ch; raw_reads_dedup_ch}

taxon_levels = "phylum class order family genus species"
taxons_affi_taxo_contigs = "all superkingdom phylum class order family genus species"

// index host if needed
if (params.host_fasta && !(params.host_bwa_index) && !(params.skip_removal_host)) {
  lastPath = params.host_fasta.lastIndexOf(File.separator)
  bwa_base = params.host_fasta.substring(lastPath+1)
  fasta_ch = file(params.host_fasta, checkIfExists: true)

  process BWAIndex {
    publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/bwa_index" , mode: 'copy'

    input:
    file fasta from fasta_ch

    output:
    file("${fasta}.*") into bwa_built

    script:
    """
    bwa index -a bwtsw $fasta
    """
  }
}

if (!(params.skip_removal_host) && !(params.host_fasta) && !(params.skip_01_clean_qc)) {
  exit 1, "You must specify --host_fasta or skip cleaning host step with option --skip_removal_host or skip all clean and qc modules with --skip_01_clean_qc"
}

if (!(params.skip_removal_host) && !(params.skip_01_clean_qc)) {
  bwa_index_ch = params.host_bwa_index ? Channel.value(file(params.host_bwa_index)) : bwa_built
}

/*
 * CLEANING.
 */

// Cutadapt.
process cutadapt {
  tag "$sampleId"

  publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/", mode: 'copy', pattern: 'cleaned_*.fastq.gz'
  publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/logs", mode: 'copy', pattern: '*_cutadapt.log'
  input:
  set sampleId, file(read1), file(read2) from raw_reads_cutadapt
  val adapter1_ch
  val adapter2_ch

  output:
  set sampleId, file("*${sampleId}*_R1.fastq.gz"), file("*${sampleId}*_R2.fastq.gz") into (cutadapt_reads_ch, cutadapt2_reads_ch, cutadapt3_reads_ch)
  file("${sampleId}_cutadapt.log") into cutadapt_log_ch_for_multiqc

  when: ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)

  script:
  if(params.skip_sickle & params.skip_removal_host) {
    // output are final cleaned files
    output_files = "-o cleaned_${sampleId}_R1.fastq.gz -p cleaned_${sampleId}_R2.fastq.gz"
  }
  else {
    //tempory files not saved in publish dir
    output_files = "-o ${sampleId}_cutadapt_R1.fastq.gz -p ${sampleId}_cutadapt_R2.fastq.gz"
  }
  """
  cutadapt -a $adapter1_ch -A $adapter2_ch $output_files -m 36 --trim-n -q 20,20 --max-n 0 \
  --cores=${task.cpus} ${read1} ${read2} > ${sampleId}_cutadapt.log
  """
}

// Sickle.
process sickle {
  tag "$sampleId"
  publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/", mode: 'copy', pattern: 'cleaned_*.fastq.gz'
  publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/logs", mode: 'copy', pattern: '*_sickle.log'

  when:
  (!params.skip_sickle) && ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)

  input:
  set sampleId, file(cutadapt_reads_R1), file(cutadapt_reads_R2) from cutadapt_reads_ch

  output:
  set sampleId, file("*${sampleId}*_R1.fastq.gz"), file("*${sampleId}*_R2.fastq.gz") into sickle_reads_ch, sickle2_reads_ch
  file("${sampleId}_single_sickle.fastq.gz") into sickle_single_ch
  file("${sampleId}_sickle.log") into sickle_log_ch_for_multiqc

  script:
  mode = params.single_end ? 'se' : 'pe'

  if(params.skip_removal_host) {
    // output are final cleaned files
    options = "-o cleaned_${sampleId}_R1.fastq.gz -p cleaned_${sampleId}_R2.fastq.gz"
  }
  else {
    //tempory files not saved in publish dir
    options = "-o ${sampleId}_sickle_R1.fastq.gz -p ${sampleId}_sickle_R2.fastq.gz"
  }
  options += " -t " + params.quality_type
  """
  sickle ${mode} -f ${cutadapt_reads_R1} -r ${cutadapt_reads_R2} $options \
  -s ${sampleId}_single_sickle.fastq.gz -g > ${sampleId}_sickle.log
  """
}

if (!params.skip_sickle) {
  sickle_reads_ch.set{intermediate_cleaned_ch}
}
else {
  cutadapt2_reads_ch.set{intermediate_cleaned_ch}
}

// WARNING: use bioinfo_bwa_samtools module.
if (!params.skip_removal_host && ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)) {
  process host_filter {
    tag "${sampleId}"
    publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/", mode: 'copy', pattern: 'cleaned_*.fastq.gz'
    publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/", mode: 'copy', pattern: '*.bam'
    publishDir "${params.outdir}/01_clean_qc/01_1_cleaned_reads/logs", mode: 'copy',
      saveAs: {filename -> 
      if (filename.indexOf(".flagstat") > 0 ) "$filename"
      else null}

    input:
    set sampleId, file(trimmed_reads_R1), file(trimmed_reads_R2) from intermediate_cleaned_ch
    file index from bwa_index_ch
    file fasta from bwa_fasta_ch

    output:
    set sampleId, file("cleaned_${sampleId}_R1.fastq.gz"), file("cleaned_${sampleId}_R2.fastq.gz") into filter_reads_ch
    file("host_filter_flagstat/${sampleId}.host_filter.flagstat") into flagstat_after_host_filter_for_multiqc_ch
    file("${sampleId}.no_filter.flagstat") into flagstat_before_filter_for_multiqc_ch

    """
    bwa mem -t ${task.cpus} ${fasta} ${trimmed_reads_R1} ${trimmed_reads_R2} > ${sampleId}.bam
    samtools view -bhS -f 12 ${sampleId}.bam > ${sampleId}.without_host.bam
    mkdir host_filter_flagstat
    samtools flagstat ${sampleId}.bam > ${sampleId}.no_filter.flagstat
    samtools flagstat ${sampleId}.without_host.bam >> host_filter_flagstat/${sampleId}.host_filter.flagstat
    bamToFastq -i ${sampleId}.without_host.bam -fq cleaned_${sampleId}_R1.fastq -fq2 cleaned_${sampleId}_R2.fastq
    gzip cleaned_${sampleId}_R1.fastq
    gzip cleaned_${sampleId}_R2.fastq
    rm ${sampleId}.bam
    rm ${sampleId}.without_host.bam
    """
  }
  filter_reads_ch.set{preprocessed_reads_ch}
}
else {
  intermediate_cleaned_ch.set{preprocessed_reads_ch}
  Channel.empty().set{flagstat_after_host_filter_for_multiqc_ch}
  Channel.empty().set{flagstat_before_filter_for_multiqc_ch}
}

preprocessed_reads_ch.into{
  clean_reads_for_fastqc_ch
  clean_reads_for_kaiju_ch
  clean_reads_for_assembly_ch
  clean_reads_for_dedup_ch
}


// FastQC on raw data
process fastqc_raw {
  tag "${sampleId}"
  publishDir "${params.outdir}/01_clean_qc/01_2_qc/fastqc_raw/", mode: 'copy'

  input:
  set sampleId, file(read1), file(read2) from raw_reads_fastqc

  output:
  file("${sampleId}/*.zip") into fastqc_raw_for_multiqc_ch
  file("${sampleId}/*.html") into fastqc_raw_ch

  when: ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)

  script:
  """
  mkdir ${sampleId} ; fastqc --nogroup --quiet -o ${sampleId} --threads ${task.cpus} ${read1} ${read2}
  """
}

// FastQC on cleaned data
process fastqc_cleaned {
  tag "${sampleId}"
  publishDir "${params.outdir}/01_clean_qc/01_2_qc/fastqc_cleaned", mode: 'copy'

  input:
  set sampleId, file(read1), file(read2) from clean_reads_for_fastqc_ch

  output:
  file("cleaned_${sampleId}/*.zip") into fastqc_cleaned_for_multiqc_ch
  file("cleaned_${sampleId}/*.html") into fastqc_cleaned_ch

  when: ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || 'structural' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)

  script:
  """
  mkdir cleaned_${sampleId}; fastqc --nogroup --quiet -o cleaned_${sampleId} --threads ${task.cpus} ${read1} ${read2}
  """
}

/*
 * TAXONOMIC CLASSIFICATION.
 */

if (!params.skip_kaiju && ('01_clean_qc' in step || '02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step) && (!params.skip_01_clean_qc)) {
  if(!params.kaiju_db_dir) {
    // Built kaiju database index.
    process index_db_kaiju {
      publishDir "${params.outdir}/01_clean_qc/01_3_taxonomic_affiliation_reads/index", mode: 'copy'

      input:
      val database from params.kaiju_db

      output:
      file("nodes.dmp") into index_kaiju_nodes_ch
      file("*.fmi") into index_kaiju_db_ch
      file("names.dmp") into index_kaiju_names_ch

      script:
      """
      wget ${database}
      file='${database}'
      fileNameDatabase=\${file##*/}
      echo \$fileNameDatabase
      tar -zxvf \$fileNameDatabase
      """
    }
  }
  else if(params.kaiju_db_dir) {
    if(file(params.kaiju_db_dir + "/kaiju_db*.fmi").size == 1) {
      index_kaiju_nodes_ch  = Channel
        .value(params.kaiju_db_dir + "/nodes.dmp")
      index_kaiju_db_ch = Channel
        .value(params.kaiju_db_dir + "/kaiju_db*.fmi")
      index_kaiju_names_ch = Channel
        .value(params.kaiju_db_dir + "/names.dmp")
    }
    else {
      exit 1, "There are more than one file ending with .fmi in ${params.kaiju_db_dir}"
    }
  }
  else {  
    exit 1, "You must specify --kaiju_db or --kaiju_db_dir or --skip_kaiju"
  }

  // Kaiju.
  process kaiju {
    tag "${sampleId}"
    publishDir "${params.outdir}/01_clean_qc/01_3_taxonomic_affiliation_reads", mode: 'copy', pattern: '*.krona.html'

    input:
    set sampleId, file(preprocessed_reads_R1), file(preprocessed_reads_R2) from clean_reads_for_kaiju_ch
    val (index_kaiju_nodes) from index_kaiju_nodes_ch
    val (index_kaiju_db) from index_kaiju_db_ch
    val (index_kaiju_names) from index_kaiju_names_ch

    output:
    file("${sampleId}_kaiju_MEM_verbose.out") into kaiju_MEM_verbose_ch
    file("${sampleId}.krona.html") into kaiju_krona_ch
    file("*.summary_*") into kaiju_summary_for_multiqc_ch
    file("*.summary_species") into kaiju_summary_species_ch
    file("*.summary_genus") into kaiju_summary_genus_ch
    file("*.summary_family") into kaiju_summary_family_ch
    file("*.summary_class") into kaiju_summary_class_ch
    file("*.summary_order") into kaiju_summary_order_ch
    file("*.summary_phylum") into kaiju_summary_phylum_ch
    file("*.pdf") into plot_ch

    script:
    """
    kaiju -z ${task.cpus} -t ${index_kaiju_nodes} -f ${index_kaiju_db} -i ${preprocessed_reads_R1} -j ${preprocessed_reads_R2} -o ${sampleId}_kaiju_MEM_verbose.out -a mem -v
    kaiju2krona -t ${index_kaiju_nodes} -n ${index_kaiju_names} -i ${sampleId}_kaiju_MEM_verbose.out -o ${sampleId}_kaiju_MEM_without_unassigned.out.krona -u
    ktImportText -o ${sampleId}.krona.html ${sampleId}_kaiju_MEM_without_unassigned.out.krona
    for i in ${taxon_levels} ;
    do
    kaiju2table -t ${index_kaiju_nodes} -n ${index_kaiju_names} -r \$i -o ${sampleId}_kaiju_MEM.out.summary_\$i ${sampleId}_kaiju_MEM_verbose.out
    done
    Generate_barplot_kaiju.py -i ${sampleId}_kaiju_MEM_verbose.out
    """
  }
  // Merge kaiju results by taxonomic ranks
  process kaiju_merge {
    tag "${sampleId}"
    publishDir "${params.outdir}/01_clean_qc/01_3_taxonomic_affiliation_reads", mode: 'copy'
  
    input:
    file(kaiju_species) from kaiju_summary_species_ch.collect()
    file(kaiju_genus) from kaiju_summary_genus_ch.collect()
    file(kaiju_family) from kaiju_summary_family_ch.collect()
    file(kaiju_class) from kaiju_summary_class_ch.collect()
    file(kaiju_order) from kaiju_summary_order_ch.collect()
    file(kaiju_phylum) from kaiju_summary_phylum_ch.collect()
  
    output:
    file("taxo_affi_reads_*.tsv") into merge_files_kaiju_ch
  
    script:
    """
    echo "${kaiju_phylum}" > phylum.txt
    echo "${kaiju_order}" > order.txt
    echo "${kaiju_class}" > class.txt
    echo "${kaiju_family}" > family.txt
    echo "${kaiju_genus}" > genus.txt
    echo "${kaiju_species}" > species.txt
    for i in ${taxon_levels} ;
    do
    merge_kaiju_results.py -f \$i".txt" -o taxo_affi_reads_\$i".tsv"
    done
    """
  }
}

else {
  Channel.empty().set{kaiju_summary_for_multiqc_ch}
}

if(params.skip_01_clean_qc) {
  raw_reads_assembly_ch.set{
   input_reads_for_assembly_ch}
  raw_reads_dedup_ch.set{
   input_reads_for_dedup_ch}
}
else {
  clean_reads_for_assembly_ch.set{
   input_reads_for_assembly_ch}
  clean_reads_for_dedup_ch.set{
   input_reads_for_dedup_ch}
}

/*
 * ASSEMBLY.
 */

// Assembly (metaspades or megahit).
process assembly {
  tag "${sampleId}"
  publishDir "${params.outdir}/02_assembly", mode: 'copy'
  label 'assembly'

  input:
  set sampleId, file(preprocessed_reads_R1), file(preprocessed_reads_R2) from input_reads_for_assembly_ch
  val spades_mem from metaspades_mem_ch

  output:
  set sampleId, file("${params.assembly}/${sampleId}.contigs.fa") into assembly_for_quast_ch, assembly_for_dedup_ch, assembly_for_filter_ch, assembly_no_filter_ch
  set sampleId, file("${params.assembly}/${sampleId}.log"), file("${params.assembly}/${sampleId}.params.txt") into logs_assembly_ch

  when: ('02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  if(params.assembly=='metaspades')
  """
  metaspades.py -t ${task.cpus} -m ${spades_mem} -1 ${preprocessed_reads_R1} -2 ${preprocessed_reads_R2} -o ${params.assembly}
  mv ${params.assembly}/scaffolds.fasta ${params.assembly}/${sampleId}.contigs.fa
  mv ${params.assembly}/spades.log ${params.assembly}/${sampleId}.log
  mv ${params.assembly}/params.txt ${params.assembly}/${sampleId}.params.txt
  """
  else if(params.assembly=='megahit')
  """
  megahit -t ${task.cpus} -1 ${preprocessed_reads_R1} -2 ${preprocessed_reads_R2} -o ${params.assembly} --out-prefix "${sampleId}"
  mv ${params.assembly}/options.json ${params.assembly}/${sampleId}.params.txt
  """
  else
  error "Invalid parameter: ${params.assembly}"
}

// Assembly metrics.
process quast {
  publishDir "${params.outdir}/02_assembly", mode: 'copy'

  input:
  set sampleId, file(assembly_file) from assembly_for_quast_ch

  output:
  file("${sampleId}_all_contigs_QC/*") into quast_assembly_ch
  file("${sampleId}_all_contigs_QC/report.tsv") into quast_assembly_for_multiqc_ch

  when: ('02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  metaquast.py --threads "${task.cpus}" --rna-finding --max-ref-number 0 --min-contig 0 "${assembly_file}" -o "${sampleId}_all_contigs_QC"
  """
}

// Reads deduplication

assembly_and_reads_ch = assembly_for_dedup_ch.join(input_reads_for_dedup_ch, remainder: true)

process reads_deduplication {
  publishDir "${params.outdir}/02_assembly", mode: 'copy', pattern: '*.fastq.gz'
  publishDir "${params.outdir}/02_assembly/logs", mode: 'copy', pattern: '*.idxstats'
  publishDir "${params.outdir}/02_assembly/logs", mode: 'copy', pattern: '*.flagstat'
  
  input:
  set sampleId, file(assembly_file), file(preprocessed_reads_R1), file(preprocessed_reads_R2) from assembly_and_reads_ch

  output:
  set sampleId, file("${sampleId}_R1_dedup.fastq.gz"), file("${sampleId}_R2_dedup.fastq.gz") into deduplicated_reads_ch, deduplicated_reads_copy_ch
  set sampleId, file("${sampleId}.count_reads_on_contigs.idxstats") into (idxstats_filter_logs_ch, idxstats_filter_logs_for_multiqc_ch)
  set sampleId, file("${sampleId}.count_reads_on_contigs.flagstat") into flagstat_filter_logs_ch
  file("${sampleId}.count_reads_on_contigs.flagstat") into flagstat_after_dedup_reads_for_multiqc_ch

  when: ('02_assembly' in step || '03_filtering' in step || '04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  mkdir logs
  bwa index ${assembly_file} -p ${assembly_file}
  bwa mem ${assembly_file} ${preprocessed_reads_R1} ${preprocessed_reads_R2} | samtools view -bS - | samtools sort -n -o ${sampleId}.sort.bam -
  samtools fixmate -m ${sampleId}.sort.bam ${sampleId}.fixmate.bam
  samtools sort -o ${sampleId}.fixmate.positionsort.bam ${sampleId}.fixmate.bam
  samtools markdup -r -S -s -f ${sampleId}.stats ${sampleId}.fixmate.positionsort.bam ${sampleId}.filtered.bam
  samtools index ${sampleId}.filtered.bam
  samtools idxstats ${sampleId}.filtered.bam > ${sampleId}.count_reads_on_contigs.idxstats
  samtools flagstat ${sampleId}.filtered.bam > ${sampleId}.count_reads_on_contigs.flagstat
  samtools sort -n -o ${sampleId}.filtered.sort.bam ${sampleId}.filtered.bam
  bedtools bamtofastq -i ${sampleId}.filtered.sort.bam -fq ${sampleId}_R1_dedup.fastq -fq2 ${sampleId}_R2_dedup.fastq
  gzip ${sampleId}_R1_dedup.fastq ; gzip ${sampleId}_R2_dedup.fastq
  rm ${sampleId}.sort.bam
  rm ${sampleId}.fixmate.bam
  rm ${sampleId}.fixmate.positionsort.bam
  rm ${sampleId}.filtered.bam
  rm ${sampleId}.filtered.sort.bam
  """
}

// Assembly filter
assembly_and_logs_ch = assembly_for_filter_ch.join(idxstats_filter_logs_ch, remainder: true)
process assembly_filter {
  publishDir "${params.outdir}/03_filtering", mode: 'copy'
    
  input:
  set sampleId, file(assembly_file), file(idxstats) from assembly_and_logs_ch
  val min_cpm from min_contigs_cpm_ch

  output:
  set sampleId, file("${sampleId}_select_contigs_cpm${min_cpm}.fasta") into select_assembly_ch
  set sampleId, file("${sampleId}_discard_contigs_cpm${min_cpm}.fasta") into discard_assembly_ch
  file("${sampleId}_select_contigs_QC/*") into quast_select_contigs_ch
  file("${sampleId}_select_contigs_QC/report.tsv") into quast_select_contigs_for_multiqc_ch

  when: ('03_filtering' in step)

  script:
  """
  Filter_contig_per_cpm.py -i ${idxstats} -f ${assembly_file} -c ${min_cpm} -s ${sampleId}_select_contigs_cpm${min_cpm}.fasta -d ${sampleId}_discard_contigs_cpm${min_cpm}.fasta
  metaquast.py --threads "${task.cpus}" --rna-finding --max-ref-number 0 --min-contig 0 "${sampleId}_select_contigs_cpm${min_cpm}.fasta" -o "${sampleId}_select_contigs_QC"
  """
}

if(!('03_filtering' in step)) {
  assembly_no_filter_ch.into{select_assembly_ch }
}

/*
 * ANNOTATION with Prokka.
 */

process prokka {
  tag "${sampleId}"

  input:
  set sampleId, file(assembly_file) from select_assembly_ch

  output:
  set sampleId, file("*") into prokka_ch
  set sampleId, file("PROKKA_${sampleId}/${sampleId}.txt") into prokka_for_multiqc_ch

  when: ('04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  prokka --metagenome --noanno --rawproduct --outdir PROKKA_${sampleId} --prefix ${sampleId} ${assembly_file} --centre X --compliant --cpus ${task.cpus}
  """
}

/*
 * RENAME contigs and genes.
 */

process rename_contigs_genes {
  tag "${sampleId}"
  publishDir "${params.outdir}/04_structural_annot", mode: 'copy'
  label 'python'

  input:
  set sampleId, file(assembly_file) from prokka_ch

  output:
  set sampleId, file("${sampleId}.annotated.fna") into prokka_renamed_fna_ch, prokka_renamed_fna_ch2, prokka_renamed_fna_for_metabat2_ch
  set sampleId, file("${sampleId}.annotated.ffn") into prokka_renamed_ffn_ch
  set sampleId, file("${sampleId}.annotated.faa") into prokka_renamed_faa_ch
  set sampleId, file("${sampleId}.annotated.gff") into prokka_renamed_gff_ch, prokka_renamed_gff_ch2
  set sampleId, file("${sampleId}_prot.len") into prot_length_ch

  when: ('04_structural_annot' in step || '05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  grep "^gnl" ${assembly_file}/${sampleId}.gff > ${sampleId}_only_gnl.gff
  Rename_contigs_and_genes.py -f ${sampleId}_only_gnl.gff -faa ${assembly_file}/${sampleId}.faa -ffn ${assembly_file}/${sampleId}.ffn -fna ${assembly_file}/${sampleId}.fna -p ${sampleId} -oGFF ${sampleId}.annotated.gff -oFAA ${sampleId}.annotated.faa -oFFN ${sampleId}.annotated.ffn -oFNA ${sampleId}.annotated.fna
  samtools faidx ${sampleId}.annotated.faa; cut -f 1,2 ${sampleId}.annotated.faa.fai > ${sampleId}_prot.len
  """
}
prokka_renamed_faa_ch.into{prokka_renamed_faa_ch2;  prokka_renamed_faa_ch4}

// ALIGNMENT OF READS AGAINST CONTIGS.
prokka_reads_ch = prokka_renamed_gff_ch.join(prokka_renamed_fna_ch, remainder: true).join(deduplicated_reads_ch, remainder: true)

process reads_alignment_on_contigs {
  tag "${sampleId}"
  publishDir "${params.outdir}/05_alignment/05_1_reads_alignment_on_contigs/$sampleId", mode: 'copy'

  input:
  set sampleId, file(gff_prokka), file(fna_prokka), file(deduplicated_reads_R1), file(deduplicated_reads_R2) from prokka_reads_ch

  output:
  set val(sampleId), file("${sampleId}.sort.bam"), file("${sampleId}.sort.bam.bai") into reads_assembly_ch, reads_assembly_ch_for_metabat2, reads_assembly_ch_for_depth
  set val(sampleId), file("${sampleId}.sort.bam.idxstats") into idxstats_ch
  set val(sampleId), file("${sampleId}_contig.bed") into contigs_bed_ch

  when: ('05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  bwa index ${fna_prokka} -p ${fna_prokka}
  bwa mem ${fna_prokka} ${deduplicated_reads_R1} ${deduplicated_reads_R2} | samtools view -bS - | samtools sort - -o ${sampleId}.sort.bam
  samtools index ${sampleId}.sort.bam
  samtools idxstats ${sampleId}.sort.bam > ${sampleId}.sort.bam.idxstats
  awk 'BEGIN {FS="\t"}; {print \$1 FS "0" FS \$2}' ${sampleId}.sort.bam.idxstats > ${sampleId}_contig.bed
  """
}

depth_on_contigs_ch = contigs_bed_ch.join(reads_assembly_ch_for_depth)

process depth_on_contigs {
  tag "${sampleId}"
  publishDir "${params.outdir}/05_alignment/05_1_reads_alignment_on_contigs/$sampleId", mode: 'copy'
  label 'mosdepth'

  input:
  set val(sampleId), file(bed) , file(bam), file(index) from depth_on_contigs_ch 
  output:
  set val(sampleId), file("${sampleId}.regions.bed.gz") into contig_depth_ch

  when: ('05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  script:
  """
  mosdepth -b ${bed} -Q20 -n -x ${sampleId} ${bam}
  """
}

// ALIGNMENT AGAINST PROTEIN DATABASE: DIAMOND.

process diamond {
  publishDir "${params.outdir}/05_alignment/05_2_database_alignment/$sampleId", mode: 'copy'
  tag "${sampleId}"

  when: ('05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  input:
  set sampleId, file(renamed_prokka_faa) from prokka_renamed_faa_ch2
  val diamond_bank from diamond_bank_ch

  output:
  set sampleId, file("${sampleId}_aln_diamond.m8") into diamond_result_ch, diamond_result_for_annot_ch

  script:
  """
  diamond blastp -p ${task.cpus} -d ${diamond_bank} -q ${renamed_prokka_faa} -o ${sampleId}_aln_diamond.m8 -f 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen stitle
  """
}

process diamond_header {
  publishDir "${params.outdir}/05_alignment/05_2_database_alignment/", mode: 'copy'

  when: ('05_alignment' in step || '06_func_annot' in step || '07_taxo_affi' in step || '08_binning' in step)

  output:
  file("head.m8") into diamond_header_ch

  script:
  """
  echo "qseqid	sseqid	pident	length	mismatch	gapopen	qstart	qend	sstart	send	evalue	bitscore	qlen	slen	stitle" > head.m8
  """
}

/*
 * CLUSTERING.
 */

// Sample clustering with CD-HIT.
process individual_cd_hit {
  tag "${sampleId}"
  publishDir "${params.outdir}/06_func_annot/06_1_clustering", mode: 'copy'
  label 'cd_hit'

  input:
  set sampleId, file(assembly_ffn_file) from prokka_renamed_ffn_ch
  val percentage_identity_cdhit from percentage_identity_ch

  output:
  file("${sampleId}.cd-hit-est.${percentage_identity_cdhit}.fasta") into individual_cd_hit_ch
  file("${sampleId}.cd-hit-est.${percentage_identity_cdhit}.fasta.clstr") into individual_cd_hit_cluster_ch
  file("${sampleId}.cd-hit-est.${percentage_identity_cdhit}.table_cluster_contigs.txt") into table_clstr_contigs_ch

  when: ('06_func_annot' in step)

  script:
  """
  cd-hit-est -c ${percentage_identity_cdhit} -i ${assembly_ffn_file} -o ${sampleId}.cd-hit-est.${percentage_identity_cdhit}.fasta -T ${task.cpus} -M ${task.mem} -d 150
  cat ${sampleId}.cd-hit-est.${percentage_identity_cdhit}.fasta.clstr | cd_hit_produce_table_clstr.py > ${sampleId}.cd-hit-est.${percentage_identity_cdhit}.table_cluster_contigs.txt
  """
}

// Global clustering with CD-HIT.
process global_cd_hit {
  publishDir "${params.outdir}/06_func_annot/06_1_clustering", mode: 'copy'
  label 'cd_hit'

  input:
  file "*" from individual_cd_hit_ch.collect()
  val percentage_identity_cdhit from percentage_identity_ch

  output:
  file("All-cd-hit-est.${percentage_identity_cdhit}.fasta") into concatenation_individual_cd_hit_ch
  file("All-cd-hit-est.${percentage_identity_cdhit}.fasta.clstr") into global_cd_hit_ch
  file("table_clstr.txt") into table_global_clstr_clstr_ch

  when: ('06_func_annot' in step)

  script:
  """
  cat * > All-cd-hit-est.${percentage_identity_cdhit}
  cd-hit-est -c ${percentage_identity_cdhit} -i All-cd-hit-est.${percentage_identity_cdhit} -o All-cd-hit-est.${percentage_identity_cdhit}.fasta -T ${task.cpus} -M {task.mem} -d 150
  cat All-cd-hit-est.${percentage_identity_cdhit}.fasta.clstr | cd_hit_produce_table_clstr.py > table_clstr.txt
  """
}

/*
 * Create channel with sample id, renamed annotation files and bam and index bam files.
*/

prokka_bam_ch = prokka_renamed_gff_ch2.join(prokka_renamed_fna_ch2, remainder: true).join(reads_assembly_ch, remainder: true)

/*
 * QUANTIFICATION
 */

// Quantification of reads on each gene in each sample.
process quantification {
  tag "${sampleId}"
  publishDir "${params.outdir}/06_func_annot/06_2_quantification", mode: 'copy'

  input:
  set sampleId, file(gff_prokka), file(fna_prokka), file(bam), file(bam_index) from prokka_bam_ch

  output:
  file("${sampleId}.featureCounts.tsv") into counts_ch
  file("${sampleId}.featureCounts.tsv.summary") into featureCounts_out_ch_for_multiqc
  file("${sampleId}.featureCounts.stdout") into featureCounts_error_ch

  when: ('06_func_annot' in step)

  script:
  """
  featureCounts -T ${task.cpus} -p -O -t gene -g ID -a ${gff_prokka} -o ${sampleId}.featureCounts.tsv ${bam} &> ${sampleId}.featureCounts.stdout
  """
}

// Create table with sum of reads for each global cluster of genes in each sample.
process quantification_table {
  publishDir "${params.outdir}/06_func_annot/06_2_quantification", mode: 'copy'
  label 'python'

  input:
  file(clusters_contigs) from table_clstr_contigs_ch.collect()
  file(global_clusters_clusters) from table_global_clstr_clstr_ch
  file(counts_files) from counts_ch.collect()

  output:
  file("Correspondence_global_clstr_genes.txt") into table_global_clstr_genes_ch
  file("Clusters_Count_table_all_samples.txt") into quantification_table_ch

  when: ('06_func_annot' in step)
  script:
  """
  ls ${clusters_contigs} | cat > List_of_contigs_files.txt
  ls ${counts_files} | cat > List_of_count_files.txt
  Quantification_clusters.py -t ${global_clusters_clusters} -l List_of_contigs_files.txt -c List_of_count_files.txt -oc Clusters_Count_table_all_samples.txt -oid Correspondence_global_clstr_genes.txt
  """
}

/*
 * FUNCTIONAL ANNOTATION OF GENES.
 */

if(params.eggnogmapper_db) {
  // Built eggNOG-mapper database.
  process eggnog_mapper_db {

    label 'eggnog'

    output:
    file "db_eggnog_mapper" into functional_annot_db_ch

    when: ('06_func_annot' in step)

    script:
    """
    mkdir db_eggnog_mapper
    /eggnog-mapper-2.0.4-rf1/download_eggnog_data.py -P -f -y --data_dir db_eggnog_mapper
    """
  }
}
else {
  if(params.eggnog_mapper_db_dir) {
    functional_annot_db_ch = Channel.fromPath(params.eggnog_mapper_db_dir).first()
  }
  else {
    if (!(params.eggnogmapper_db) & !(params.eggnog_mapper_db_dir) & "06_func_annot" in step){
      exit 1, "You must specify --eggnogmapper_db or --eggnog_mapper_db_dir"
    }
    if (!(params.eggnogmapper_db) & !(params.eggnog_mapper_db_dir) & !("06_func_annot" in step)){
      Channel.empty().set{functional_annot_db_ch}
    }
  }
}

// Run eggNOG-mapper.
process eggnog_mapper {
  publishDir "${params.outdir}/06_func_annot/06_3_functional_annotation", mode: 'copy'
  label 'eggnog'

  input:
  set sampleId, file(renamed_prokka_faa) from prokka_renamed_faa_ch4
  file(db) from functional_annot_db_ch

  output:
  file "${sampleId}_diamond_one2one.emapper.seed_orthologs" into functional_annot_seed_ch
  file "${sampleId}_diamond_one2one.emapper.annotations" into functional_annot_ch

  when: ('06_func_annot' in step)

  script:
  """
  /eggnog-mapper-2.0.4-rf1/emapper.py -i ${renamed_prokka_faa} --output ${sampleId}_diamond_one2one -m diamond --cpu ${task.cpus} --data_dir ${db} --target_orthologs one2one
  """
}

// Best hits (best bitscore) of diamond results.
process best_hits_diamond {
  publishDir "${params.outdir}/06_func_annot/06_3_functional_annotation", mode: 'copy'

  input:
  set sampleId, file(diamond_file) from diamond_result_for_annot_ch

  output:
  file "${sampleId}.best_hit" into diamond_best_hits_ch

  when: ('06_func_annot' in step)

  script:
  """
  filter_diamond_hits.py -o ${sampleId}.best_hit ${diamond_file}
  """
}

// Merge eggNOG-mapper output files and quantification table.
process merge_quantif_and_functional_annot {
  publishDir "${params.outdir}/06_func_annot/06_3_functional_annotation", mode: 'copy'

  input:
  file(functionnal_annotations_files) from functional_annot_ch.collect()
  file(quantification_table) from quantification_table_ch
  file(diamond_files) from diamond_best_hits_ch.collect()

  output:
  file "Quantifications_and_functional_annotations.tsv" into quantification_and_functional_annotation_ch

  when: ('06_func_annot' in step)

  script:
  """
  awk '{
      if(NR == 1) {
          print \$0 "\t" "sum"}
      else {
          for (i=1; i<=NF; i++)
          {
              if (i == 1)
              {
                  sum = O;
              }
              else {
                  sum = sum + \$i;
              }
          }
          print \$0 "\t" sum
      }
  }' ${quantification_table} > ${quantification_table}.sum
  ls ${functionnal_annotations_files} | cat > List_of_functionnal_annotations_files.txt
  ls ${diamond_files} | cat > List_of_diamond_files.txt
  merge_abundance_and_functional_annotations.py -t ${quantification_table}.sum -f List_of_functionnal_annotations_files.txt -d List_of_diamond_files.txt -o Quantifications_and_functional_annotations.tsv
  """
}

// Merge eggNOG-mapper output files and quantification table.
process make_functional_annotation_tables {
  publishDir "${params.outdir}/06_func_annot/06_3_functional_annotation", mode: 'copy'

  input:
  file(quantif_and_functionnal_annotations) from quantification_and_functional_annotation_ch

  output:
  file "*" into quantif_by_function_ch

  when: ('06_func_annot' in step)

  script:
  """
  quantification_by_functional_annotation.py -i ${quantif_and_functionnal_annotations}
  """
}

 // TAXONOMIC AFFILIATION OF CONTIGS.

 // Download taxonomy if taxonomy_dir is not given.
if(!params.taxonomy_dir) {
  process download_taxonomy_db {

  when:
  ('07_taxo_affi' in step)

  input:
  val accession2taxid_ch
  val taxdump_ch

  output:
  set file("*taxid*"), file ("*taxdump*") into taxonomy_ch

  script:
  """
  wget ${accession2taxid_ch}
  file='${accession2taxid_ch}'
  fileName=\${file##*/}
  echo \$fileName
  gunzip \$fileName
  wget ${taxdump_ch}
  file_taxdump='${taxdump_ch}'
  fileName_taxdump=\${file_taxdump##*/}
  echo \$fileName_taxdump
  mkdir taxdump; mv \$fileName_taxdump taxdump; cd taxdump ; tar xzvf \$fileName_taxdump
  """
  }
}
else if(params.taxonomy_dir) {
  assert file(params.taxonomy_dir + '/prot.accession2taxid').exists()
  assert file(params.taxonomy_dir + '/taxdump').exists()
  accession2taxid_ch = Channel
    .fromPath(params.taxonomy_dir + '/prot.accession2taxid')
  taxdump_ch = Channel
    .fromPath(params.taxonomy_dir + '/taxdump')
  taxonomy_ch = accession2taxid_ch.combine(taxdump_ch)
}
else {
  exit 1, "You must specify [--accession2taxid and --taxdump] or --taxonomy_dir"
}

/*
 * Create channel with sample id, diamond files and desman length files, idxstats and depth files.
*/

diamond_parser_input_ch = diamond_result_ch.join(prot_length_ch, remainder: true).join(idxstats_ch, remainder: true).join(contig_depth_ch, remainder: true)

// Python parser.
process diamond_parser {
  tag "$sampleId"
  publishDir "${params.outdir}/07_taxo_affi/$sampleId", mode: 'copy'
  label 'python'

  when: ('07_taxo_affi' in step)

  input:
  set file(accession2taxid), file(taxdump) from taxonomy_ch.collect()
  set sampleId, file(diamond_file), file(prot_len), file(idxstats), file(depth) from diamond_parser_input_ch

  output:
  set sampleId, file("${sampleId}.percontig.tsv") into taxo_percontig_ch
  set sampleId, file("${sampleId}.pergene.tsv") into taxo_pergene_ch
  set sampleId, file("${sampleId}.warn.tsv") into taxo_warn_ch
  set sampleId, file("graphs") into taxo_graphs_ch
  file("${sampleId}_quantif_percontig.tsv") into quantif_percontig_ch
  file("${sampleId}_quantif_percontig_by_superkingdom.tsv") into quantif_percontig_superkingdom_ch
  file("${sampleId}_quantif_percontig_by_phylum.tsv") into quantif_percontig_phylum_ch
  file("${sampleId}_quantif_percontig_by_order.tsv") into quantif_percontig_order_ch
  file("${sampleId}_quantif_percontig_by_class.tsv") into quantif_percontig_class_ch
  file("${sampleId}_quantif_percontig_by_family.tsv") into quantif_percontig_family_ch
  file("${sampleId}_quantif_percontig_by_genus.tsv") into quantif_percontig_genus_ch
  file("${sampleId}_quantif_percontig_by_species.tsv") into quantif_percontig_species_ch

  script:
  """
  aln2taxaffi.py -a ${accession2taxid} --taxonomy ${taxdump} -o ${sampleId} ${diamond_file} ${prot_len}
  merge_contig_quantif_perlineage.py -i ${idxstats} -c ${sampleId}.percontig.tsv -m ${depth} -o ${sampleId}_quantif_percontig
  """
}

process quantif_and_taxonomic_table_contigs {
  publishDir "${params.outdir}/07_taxo_affi", mode: 'copy'
  label 'python'

  when: ('07_taxo_affi' in step)

  input:
  file(files_all) from quantif_percontig_ch.collect()
  file(files_superkingdom) from quantif_percontig_superkingdom_ch.collect()
  file(files_phylum) from quantif_percontig_phylum_ch.collect()
  file(files_order) from quantif_percontig_order_ch.collect()
  file(files_class) from quantif_percontig_class_ch.collect()
  file(files_family) from quantif_percontig_family_ch.collect()
  file(files_genus) from quantif_percontig_genus_ch.collect()
  file(files_species) from quantif_percontig_species_ch.collect()

  output:
  file("quantification_by_contig_lineage*.tsv") into quantif_by_lineage_contigs_ch

  script:
  """
    echo "${files_all}" > all.txt
    echo "${files_superkingdom}" > superkingdom.txt
    echo "${files_phylum}" > phylum.txt
    echo "${files_order}" > order.txt
    echo "${files_class}" > class.txt
    echo "${files_family}" > family.txt
    echo "${files_genus}" > genus.txt
    echo "${files_species}" > species.txt
    for i in ${taxons_affi_taxo_contigs} ;
    do
    quantification_by_contig_lineage.py -i \$i".txt" -o quantification_by_contig_lineage_\$i".tsv"
    done
  """
}

/*
 * Binning of contigs - from nf-core/mag
 */

reads_assembly_ch_for_metabat2 = reads_assembly_ch_for_metabat2.groupTuple(by:[0,1]).join(prokka_renamed_fna_for_metabat2_ch)

reads_assembly_ch_for_metabat2 = reads_assembly_ch_for_metabat2.dump(tag:'reads_assembly_ch_for_metabat2')

process metabat {
  tag "$sampleId"
  publishDir "${params.outdir}/", mode: 'copy',
    saveAs: {filename -> (filename.indexOf(".bam") == -1 && filename.indexOf(".fastq.gz") == -1) ? "08_binning/08_1_binning/$filename" : null}
  label 'binning'

  input:
  set val(sampleId), file(bam), file(index), file(assembly) from reads_assembly_ch_for_metabat2
  val(min_size) from params.min_contig_size

  output:
  set val(sampleId), file("MetaBAT2/*") into metabat_bins mode flatten
  set val(sampleId), file("MetaBAT2/*") into metabat_bins_for_cat
  set val(sampleId), file("MetaBAT2/*") into metabat_bins_quast_bins

  when: ('08_binning' in step)

  script:
  def name = "${sampleId}"
  """
  jgi_summarize_bam_contig_depths --outputDepth depth.txt ${bam}
  metabat2 -t "${task.cpus}" -i "${assembly}" -a depth.txt -o "MetaBAT2/${name}" -m ${min_size}
  #if bin folder is empty
  if [ -z \"\$(ls -A MetaBAT2)\" ]; then 
      cp ${assembly} MetaBAT2/${assembly}
  fi
  """
}

process busco_download_db {
  tag "${database.baseName}"
  label 'binning'

  input:
  file(database) from file_busco_db

  output:
  set val("${database.toString().replace(".tar.gz", "")}"), file("buscodb/*") into busco_db

  when: ('08_binning' in step) && !(params.skip_busco)

  script:
  """
  mkdir buscodb
  tar -xf ${database} -C buscodb
  """
}

metabat_bins
  .combine(busco_db)
  .set { metabat_db_busco }

/*
 * BUSCO: Quantitative measures for the assessment of genome assembly
 */
process busco {
  tag "${assembly}"
  publishDir "${params.outdir}/08_binning/08_2_QC/BUSCO/", mode: 'copy'
  label 'binning'
  
  input:
  set val(sampleId), file(assembly), val(db_name), file(db) from metabat_db_busco

  output:
  file("short_summary_${assembly}.txt") into (busco_summary_to_multiqc, busco_summary_to_plot)
  val("$sampleId") into busco_assembler_sample_to_plot
  file("${assembly}_busco_log.txt")
  file("${assembly}_buscos.faa")
  file("${assembly}_buscos.fna")

  when: ('08_binning' in step) && !(params.skip_busco)
  
  script:
  if(workflow.profile.toString().indexOf("conda") == -1) {
    """
    cp -r /opt/conda/pkgs/augustus*/config augustus_config/
    export AUGUSTUS_CONFIG_PATH=augustus_config
    run_BUSCO.py \
        --in ${assembly} \
        --lineage_path $db_name \
        --cpu "${task.cpus}" \
        --blast_single_core \
        --mode genome \
        --out ${assembly} \
        >${assembly}_busco_log.txt
    cp run_${assembly}/short_summary_${assembly}.txt short_summary_${assembly}.txt
    for f in run_${assembly}/single_copy_busco_sequences/*faa; do 
        [ -e "\$f" ] && cat run_${assembly}/single_copy_busco_sequences/*faa >${assembly}_buscos.faa || touch ${assembly}_buscos.faa
        break
    done
    for f in run_${assembly}/single_copy_busco_sequences/*fna; do 
        [ -e "\$f" ] && cat run_${assembly}/single_copy_busco_sequences/*fna >${assembly}_buscos.fna || touch ${assembly}_buscos.fna
        break
    done
    """
  }
  else {
    """
    run_BUSCO.py \
        --in ${assembly} \
        --lineage_path $db_name \
        --cpu "${task.cpus}" \
        --blast_single_core \
        --mode genome \
        --out ${assembly} \
        >${assembly}_busco_log.txt
    cp run_${assembly}/short_summary_${assembly}.txt short_summary_${assembly}.txt
    for f in run_${assembly}/single_copy_busco_sequences/*faa; do 
        [ -e "\$f" ] && cat run_${assembly}/single_copy_busco_sequences/*faa >${assembly}_buscos.faa || touch ${assembly}_buscos.faa
        break
    done
    for f in run_${assembly}/single_copy_busco_sequences/*fna; do 
        [ -e "\$f" ] && cat run_${assembly}/single_copy_busco_sequences/*fna >${assembly}_buscos.fna || touch ${assembly}_buscos.fna
        break
    done
    """
  }
}

process busco_plot { 
  publishDir "${params.outdir}/08_binning/08_2_QC/", mode: 'copy'
  label 'binning'

  input:
  file(summaries) from busco_summary_to_plot.collect()
  val(assemblersample) from busco_assembler_sample_to_plot.collect()

  output:
  file("*busco_figure.png")
  file("BUSCO/*busco_figure.R")
  file("BUSCO/*busco_summary.txt")
  file("busco_summary.txt") into busco_summary

  when: ('08_binning' in step) && !(params.skip_busco)

  script:
  def assemblersampleunique = assemblersample.unique()
  """
  #for each assembler and sample:
  assemblersample=\$(echo \"$assemblersampleunique\" | sed 's/[][]//g')
  IFS=', ' read -r -a assemblersamples <<< \"\$assemblersample\"
  mkdir BUSCO
  for name in \"\${assemblersamples[@]}\"; do
      mkdir \${name}
      cp short_summary_\${name}* \${name}/
      generate_plot.py --working_directory \${name}
      
      cp \${name}/busco_figure.png \${name}-busco_figure.png
      cp \${name}/busco_figure.R \${name}-busco_figure.R
      summary_busco.py \${name}/short_summary_*.txt >BUSCO/\${name}-busco_summary.txt
  done
  cp *-busco_figure.R BUSCO/
  summary_busco.py short_summary_*.txt >busco_summary.txt
  """
}

process quast_bins {
  tag "$sampleId"
  publishDir "${params.outdir}/08_binning/08_2_QC/", mode: 'copy'
  label 'binning'

  input:
  set val(sampleId), file(assembly) from metabat_bins_quast_bins

  output:
  file("QUAST/*")
  file("QUAST/*-quast_summary.tsv") into quast_bin_summaries

  when: ('08_binning' in step)

  script:
  """
  ASSEMBLIES=\$(echo \"$assembly\" | sed 's/[][]//g')
  IFS=', ' read -r -a assemblies <<< \"\$ASSEMBLIES\"
  
  for assembly in \"\${assemblies[@]}\"; do
      metaquast.py --threads "${task.cpus}" --max-ref-number 0 --rna-finding --gene-finding -l "\${assembly}" "\${assembly}" -o "QUAST/\${assembly}"
      if ! [ -f "QUAST/${sampleId}-quast_summary.tsv" ]; then 
          cp "QUAST/\${assembly}/transposed_report.tsv" "QUAST/${sampleId}-quast_summary.tsv"
      else
          tail -n +2 "QUAST/\${assembly}/transposed_report.tsv" >> "QUAST/${sampleId}-quast_summary.tsv"
      fi
  done    
  """
}

process merge_quast_and_busco {
  publishDir "${params.outdir}/08_binning/08_2_QC/", mode: 'copy'
  label 'binning'

  input:
  file(quast_bin_sum) from quast_bin_summaries.collect()
  file(busco_sum) from busco_summary

  output:
  file("quast_and_busco_summary.tsv")
  file("quast_summary.tsv")

  when: ('08_binning' in step) && !(params.skip_busco)

  script:
  """
  QUAST_BIN=\$(echo \"$quast_bin_sum\" | sed 's/[][]//g')
  IFS=', ' read -r -a quast_bin <<< \"\$QUAST_BIN\"
  
  for quast_file in \"\${quast_bin[@]}\"; do
      if ! [ -f "quast_summary.tsv" ]; then 
          cp "\${quast_file}" "quast_summary.tsv"
      else
          tail -n +2 "\${quast_file}" >> "quast_summary.tsv"
      fi
  done   
  combine_tables.py $busco_sum quast_summary.tsv >quast_and_busco_summary.tsv
  """
}

/*
 * CAT: Bin Annotation Tool (BAT) are pipelines for the taxonomic
 * classification of long DNA sequences and metagenome assembled genomes
 * (MAGs/bins) - from nf-core/mag
 */
process cat_db {
  tag "${database.baseName}"
  label 'binning'

  input:
  file(database) from file_cat_db

  output:
  set val("${database.toString().replace(".tar.gz", "")}"), file("database/*"), file("taxonomy/*") into cat_db

  when: ('08_binning' in step)

  script:
  """
  mkdir catDB
  tar -xf ${database} -C catDB
  mv `find catDB/ -type d -name "*taxonomy*"` taxonomy/
  mv `find catDB/ -type d -name "*CAT_database*"` database/
  """
}

metabat_bins_for_cat
  .combine(cat_db)
  .set { cat_input }

process cat {
  tag "${sampleId}-${db_name}"
  publishDir "${params.outdir}/08_binning/08_3_taxonomy/", mode: 'copy',
  saveAs: {filename ->
    if (filename.indexOf(".names.txt") > 0) filename
    else "raw/$filename"
  }

  input:
  set val(sampleId), file("bins/*"), val(db_name), file("database/*"), file("taxonomy/*") from cat_input

  output:
  file("*.ORF2LCA.txt") optional true
  file("*.names.txt") optional true
  file("*.predicted_proteins.faa") optional true
  file("*.predicted_proteins.gff") optional true
  file("*.log") optional true
  file("*.bin2classification.txt") optional true
  stdout into stdout_ch

  when: ('08_binning' in step)

  script:
  """
  COUNT=`ls -1 bins/*.fa 2>/dev/null | wc -l`
  if [ \$COUNT != 0 ]
  then
      CAT bins -b "bins/" -d database/ -t taxonomy/ -n "${task.cpus}" -s .fa --top 6 -o "${sampleId}" --I_know_what_Im_doing > stdout_CAT.txt
      CAT add_names -i "${sampleId}.ORF2LCA.txt" -o "${sampleId}.ORF2LCA.names.txt" -t taxonomy/ >> stdout_CAT.txt
      CAT add_names -i "${sampleId}.bin2classification.txt" -o "${sampleId}.bin2classification.names.txt" -t taxonomy/ >> stdout_CAT.txt
  else
      echo "Sample ${sampleId}: no bins found, it is impossible to make taxonomic affiliation of bins."
  fi
  """
}

// Print error message.
stdout_ch.subscribe {
  log.info(it)
}

/*
 * SOFTWARES VERSIONS.
 */
 
process get_software_versions {
  publishDir "${params.outdir}/pipeline_info", mode: 'copy',
  saveAs: {filename ->
    if (filename.indexOf(".csv") > 0) filename
    else null
  }

  output:
  file 'software_versions_mqc.yaml' into software_versions_yaml
  file "software_versions.csv"

  script:
  """
  echo $workflow.manifest.version > v_pipeline.txt
  echo $workflow.manifest.nextflowVersion > v_nextflow.txt
  echo \$(bwa 2>&1) &> v_bwa.txt
  cutadapt --version &> v_cutadapt.txt
  sickle --version &> v_sickle.txt
  ktImportText &> v_kronatools.txt
  python --version &> v_python.txt
  echo \$(cd-hit -h 2>&1) > v_cdhit.txt
  featureCounts -v &> v_featurecounts.txt
  diamond help &> v_diamond.txt
  multiqc --version &> v_multiqc.txt
  fastqc --version &> v_fastqc.txt
  megahit --version &> v_megahit.txt
  spades.py --version &> v_spades.txt
  quast -v &> v_quast.txt
  prokka -v &> v_prokka.txt
  echo \$(kaiju -h 2>&1) > v_kaiju.txt
  samtools --version &> v_samtools.txt
  bedtools --version &> v_bedtools.txt
  scrape_software_versions.py > software_versions_mqc.yaml
  """
}

process multiqc {
  publishDir "${params.outdir}/MultiQC", mode: 'copy'

  input:
  file multiqc_config from multiqc_config_ch
  file ('*') from cutadapt_log_ch_for_multiqc.collect().ifEmpty([])
  file ('*') from sickle_log_ch_for_multiqc.collect().ifEmpty([])
  file ('*') from fastqc_raw_for_multiqc_ch.collect().ifEmpty([])
  file ('*') from fastqc_cleaned_for_multiqc_ch.collect().ifEmpty([])
  file("*_select_contigs_QC/*") from quast_select_contigs_for_multiqc_ch.collect().ifEmpty([])
  file("*_all_contigs_QC/*") from quast_assembly_for_multiqc_ch.collect().ifEmpty([])
  file("*") from prokka_for_multiqc_ch.collect().ifEmpty([])
  file("*") from kaiju_summary_for_multiqc_ch.collect().ifEmpty([])
  file("*") from featureCounts_out_ch_for_multiqc.collect().ifEmpty([])
  file ('software_versions/*') from software_versions_yaml.collect().ifEmpty([])
  file ('*') from busco_summary_to_multiqc.collect().ifEmpty([])
  file("host_filter_flagstat/*") from flagstat_after_host_filter_for_multiqc_ch.collect().ifEmpty([])
  file("*") from flagstat_before_filter_for_multiqc_ch.collect().ifEmpty([])
  file("*") from flagstat_after_dedup_reads_for_multiqc_ch.collect().ifEmpty([])

  output:
  file "multiqc_report.html" into ch_multiqc_report
  
  script:
  """
  multiqc . --config $multiqc_config -m custom_content -m fastqc -m cutadapt -m sickle -m kaiju -m quast -m prokka -m featureCounts -m busco -m samtools
  """
}
