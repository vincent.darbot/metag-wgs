#!/usr/bin/env python
from __future__ import print_function
from collections import OrderedDict
import re

regexes = {
    'metagWGS': ['v_pipeline.txt', r"(\S+)"],
    'Nextflow': ['v_nextflow.txt', r"(\S+)"],
    'BWA': ['v_bwa.txt', r"Version: (\S+)"],
    'Cutadapt': ['v_cutadapt.txt', r"(\S+)"],
    'Sickle': ['v_sickle.txt', r"sickle version (\S+)"],
    'KronaTools': ['v_kronatools.txt', r"KronaTools (\S+)"],
    'Python': ['v_python.txt', r"Python (\S+)"],
    'CD-HIT': ['v_cdhit.txt', r"CD-HIT version (\S+)"],
    'FeatureCounts': ['v_featurecounts.txt', r"featureCounts v(\S+)"],
    'Diamond': ['v_diamond.txt', r"diamond v(\S+)"],
    'MultiQC': ['v_multiqc.txt', r"multiqc, version (\S+)"],
    'FastQC': ['v_fastqc.txt', r"FastQC v(\S+)"],
    'Megahit': ['v_megahit.txt', r"MEGAHIT v(\S+)"],
    'SPAdes': ['v_spades.txt', r"SPAdes genome assembler v(\S+)"],
    'Quast': ['v_quast.txt', r"QUAST v(\S+)"],
    'Prokka': ['v_prokka.txt', r"prokka (\S+)"],
    'Kaiju': ['v_kaiju.txt', r"Kaiju (\S+)"],
    'Samtools': ['v_samtools.txt', r"samtools (\S+)"],
    'Bedtools': ['v_bedtools.txt', r"bedtools v(\S+)"]
}
results = OrderedDict()
results['metagWGS'] = '<span style="color:#999999;\">N/A</span>'
results['Nextflow'] = '<span style="color:#999999;\">N/A</span>'
results['BWA'] = '<span style="color:#999999;\">N/A</span>'
results['Cutadapt'] = '<span style="color:#999999;\">N/A</span>'
results['Sickle'] = '<span style="color:#999999;\">N/A</span>'
results['KronaTools'] = '<span style="color:#999999;\">N/A</span>'
results['Python'] = '<span style="color:#999999;\">N/A</span>'
results['CD-HIT'] = '<span style="color:#999999;\">N/A</span>'
results['FeatureCounts'] = '<span style="color:#999999;\">N/A</span>'
results['Diamond'] = '<span style="color:#999999;\">N/A</span>'
results['MultiQC'] = '<span style="color:#999999;\">N/A</span>'
results['FastQC'] = '<span style="color:#999999;\">N/A</span>'
results['Megahit'] = '<span style="color:#999999;\">N/A</span>'
results['SPAdes'] = '<span style="color:#999999;\">N/A</span>'
results['Quast'] = '<span style="color:#999999;\">N/A</span>'
results['Prokka'] = '<span style="color:#999999;\">N/A</span>'
results['Kaiju'] = '<span style="color:#999999;\">N/A</span>'
results['Samtools'] = '<span style="color:#999999;\">N/A</span>'
results['Bedtools'] = '<span style="color:#999999;\">N/A</span>'

# Search each file using its regex
for k, v in regexes.items():
    with open(v[0]) as x:
        versions = x.read()
        match = re.search(v[1], versions)
        if match:
            results[k] = "v{}".format(match.group(1))

# Remove software set to false in results
for k in results:
    if not results[k]:
        del(results[k])

# Dump to YAML
print('''
id: 'software_versions'
section_name: 'metagWGS Software Versions'
section_href: 'https://forgemia.inra.fr/genotoul-bioinfo/metagwgs'
plot_type: 'html'
description: 'are collected at run time from the software output.'
data: |
    <dl class="dl-horizontal">
''')
for k, v in results.items():
    print("        <dt>{}</dt><dd><samp>{}</samp></dd>".format(k, v))
print("    </dl>")

# Write out regexes as csv file:
with open('software_versions.csv', 'w') as f:
    for k, v in results.items():
        f.write("{}\t{}\n".format(k, v))
