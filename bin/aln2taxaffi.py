#!/usr/bin/env python

"""----------------------------------------------------------------------------
  Script Name: aln2taxaffi.py
  Description:
  Input files: File with correspondence between accession ids and taxon ids, \
  taxonomy directory, diamond output file (.m8) and \
  output file from DESMAN Lengths.py script (.len)
  Created By:  Celine Noirot
  Date:        2019-09-06
-------------------------------------------------------------------------------
"""

# Metadata
__author__ = 'Celine Noirot \
- Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

# Status: dev

# Modules importation
try:
    import argparse
    import pandas as p
    import re
    import sys
    import os
    import operator
    from collections import defaultdict
    from collections import OrderedDict
    from collections import Counter
    from matplotlib import pyplot
except ImportError as error:
    print(error)
    exit(1)

# Variables

# These are identities normalized with query coverage:
MIN_IDENTITY_TAXA = (0.40,0.50,0.60,0.70,0.80,0.90,0.95)

# Fraction of weights needed to assign at a specific level,
# a measure of concensus at that level.
MIN_FRACTION = 0.9

################################################
# Functions for taxonomy taxdump.tar.gz
################################################

# Define taxonomy variable
global d_taxonomy
d_taxonomy = {}

# Define taxonomy main levels
global main_level
main_level = \
["superkingdom", "phylum", "class", "order", "family", "genus", "species"]

##### SAME AS Script_renameVcn.py
prot_prefix = "Prot_"

# Definition of the class Node
class Node:
    def __init__(self):
        self.tax_id = 0       # Number of the tax id.
        self.parent = 0       # Number of the parent of this node
        self.children = []    # List of the children of this node
        self.tip = 0          # Tip=1 if it's a terminal node, 0 if not.
        self.name = ""        # Name of the node: taxa if it's a terminal node,
                              # numero if not.
        self.level = "None"
    def genealogy(self):      # Trace genealogy from root to leaf
        ancestors = []        # Initialize the list of all nodes
                              # from root to leaf.
        tax_id = self.tax_id  # Define leaf
        while 1:
            if tax_id in d_taxonomy:
                ancestors.append(tax_id)
                tax_id = d_taxonomy[tax_id].parent
            else:
                break
            if tax_id == "1":
                # If it is the root, we reached the end.
                # Add it to the list and break the loop
                ancestors.append(tax_id)
                break
        return ancestors       # Return the list
    def fullnamelineage(self): # Trace genealogy from root to leaf
        ancestors = []         # Initialise the list of all nodes
                               # from root to leaf.
        tax_id = self.tax_id   # Define leaf
        while 1:
            if tax_id in d_taxonomy:
                ancestors.append(d_taxonomy[tax_id].name)
                tax_id = d_taxonomy[tax_id].parent
            else:
                break
            if tax_id == "1":
                break
        ancestors.reverse()
        return "; ".join(ancestors) # Return the list
    def genealogy_main_level(self):
        ancestors = ["None"] * 7    # Initialise the list of all nodes
                                    # from root to leaf.
        tax_id = self.tax_id
        while 1:
            if tax_id in d_taxonomy:
                cur_level = d_taxonomy[tax_id].level
                if cur_level in main_level :
                    ancestors[main_level.index(cur_level)] = tax_id
                tax_id = d_taxonomy[tax_id].parent
            else:
                break
            if tax_id == "1":
                # If it is the root, we reached the end.
                break
        return ancestors # Return the list
    def lineage_main_level(self):
        ancestors = ["None"] * 7    # Initialise the list of all nodes
                                    # from root to leaf.
        ancestors_tax_id = ["None"] * 7    # Initialise the list of all nodes
        tax_id = self.tax_id
        while 1:
            if tax_id in d_taxonomy:
                cur_level = d_taxonomy[tax_id].level
                if cur_level in main_level :
                    ancestors[main_level.index(cur_level)] = d_taxonomy[tax_id].name
                    ancestors_tax_id [main_level.index(cur_level)] = str(tax_id)
                tax_id = d_taxonomy[tax_id].parent
            else:
                break
            if tax_id == "1":
                # If it is the root, we reached the end.
                break
        return ("; ".join(ancestors), "; ".join(ancestors_tax_id))# Return the two lists

# Function to find common ancestor between two nodes or more
def common_ancestor(node_list):
    global d_taxonomy
    # Define the whole genealogy of the first node
    list1 = d_taxonomy[node_list[0]].genealogy()
    for node in node_list:
        # Define the whole genealogy of the second node
        list2 = d_taxonomy[node].genealogy()
        ancestral_list = []
        for i in list1:
            if i in list2: # Identify common nodes between the two genealogy
                ancestral_list.append(i)
        list1 = ancestral_list # Reassing ancestral_list to list 1.
    # Finally, the first node of the ancestra_list is the common ancestor
    # of all nodes.
    common_ancestor = ancestral_list[0]
    # Return a node
    return common_ancestor

def load_taxonomy(directory):
    # Load taxonomy
    global d_taxonomy

    # Load names definition
    d_name_by_tax_id = {}          # Initialize dictionary with TAX_ID:NAME
    d_name_by_tax_id_reverse = {}  # Initialize dictionary with NAME:TAX_ID

    # Load  NCBI names file ("names.dmp")
    with open(os.path.join(directory, "names.dmp"), "r") as name_file:
        for line in name_file:
            line = line.rstrip().replace("\t", "")
            tab = line.split("|")
            if tab[3] == "scientific name":
                tax_id, name = tab[0], tab[1]     # Assign tax_id and name ...
                d_name_by_tax_id[tax_id] = name          # ... and load them
                d_name_by_tax_id_reverse[name] = tax_id  # ... into dictionaries


    # Load taxonomy NCBI file ("nodes.dmp")
    with open(os.path.join(directory, "nodes.dmp"), "r") as taxonomy_file :
        for line in taxonomy_file:
            line = line.rstrip().replace("\t", "")
            tab = line.split("|")

            tax_id = str(tab[0].strip())
            tax_id_parent = str(tab[1].strip())
            division = str(tab[4].strip())
            # Define name of the taxid
            name = "unknown"
            if tax_id in d_name_by_tax_id:
                name = d_name_by_tax_id[tax_id]
            if tax_id not in d_taxonomy:
                d_taxonomy[tax_id] = Node()
            d_taxonomy[tax_id].tax_id = tax_id        # Assign tax_id
            d_taxonomy[tax_id].parent = tax_id_parent # Assign tax_id parent
            d_taxonomy[tax_id].name = name            # Assign name
            d_taxonomy[tax_id].level = str(tab[2].strip()) # Assign level
            if  tax_id_parent in d_taxonomy:
                children = d_taxonomy[tax_id].children  # If parent is already in the object
                children.append(tax_id)                    # ... we found its children
                d_taxonomy[tax_id].children = children  # ... so add them to the parent.
################################################
# END Functions for taxonomy taxdump.tar.gz
################################################

def read_query_length_file(query_length_file):
    lengths = {}
    for line in open(query_length_file):
        (queryid, length) = line.rstrip().split("\t")
        lengths[queryid] = float(length)
    return lengths

def read_blast_input(blastinputfile,lengths,min_identity,max_matches,min_coverage):
#c1.Prot_00001	EFK63346.1	100.0	85	0	0	1	85	62	146	1.6e-36	158.3	85	\
# 146	EFK63346.1 LOW QUALITY PROTEIN: hypothetical protein HMPREF9008_04720, partial [Parabacteroides sp. 20_3]

    #queryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount,
    #queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore, queryLength, subjectLength, subjectTitle

    matches = defaultdict(list)
    accs = Counter()
    nmatches = Counter();

    for line in open(blastinputfile):
        (queryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount, \
        queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore, queryLength, subjectLength, subjectTitle) \
        = line.rstrip().split("\t")
        if subjectId.startswith("gi|") :
            m = re.search(r"gi\|.*?\|.*\|(.*)\|", subjectId)
            acc = m.group(1)
        else :
            acc = subjectId
        qLength = lengths[queryId]
        alnLength_in_query = abs(int(queryEnd) - int(queryStart)) + 1
        fHit = float(alnLength_in_query) / qLength
        coverage = fHit * 100
        fHit *= float(percIdentity) / 100.0
        fHit = min(1.0,fHit)
        #hits[queryId] = hits[queryId] + 1
        if float(percIdentity) > min_identity and nmatches[queryId] < max_matches and float(coverage) > min_coverage:
            matches[queryId].append((acc, fHit))
            nmatches[queryId] += 1
            accs[acc] +=1

    return (OrderedDict(sorted(matches.items(), key = lambda t: t[0])), list(accs.keys()))


def map_accessions(accs, mapping_file):
    first = True
    mappings = dict([(acc, -1) for acc in accs])
    with open(mapping_file) as mapping_fh:
        for line in mapping_fh:
            _, acc_ver, taxid, _ = line.split("\t")
            # Only add taxids for the given acc
            if acc_ver in mappings:
                mappings[acc_ver] = int(taxid)
    return mappings


def get_consensus (collate_table):
    #From collapse_hit retrieve consensus tax_id and lineage
    #Compute best lineage consensus
    for depth in range(6, -1, -1):
        collate = collate_table[depth]
        dWeight = sum(collate.values())
        sortCollate = sorted(list(collate.items()), key = operator.itemgetter(1), reverse = True)
        nL = len(collate)
        if nL > 0:
            dP = 0.0
            if dWeight > 0.0:
                dP = float(sortCollate[0][1]) / dWeight
                if dP > MIN_FRACTION:
                    (fullnamelineage_text, fullnamelineage_ids) = d_taxonomy[str(sortCollate[0][0])].lineage_main_level()
                    tax_id_keep = str(sortCollate[0][0])
                    return (tax_id_keep, fullnamelineage_text, fullnamelineage_ids)
    return (1,"Unable to found taxonomy consensus",1)

def main(argv):

    parser = argparse.ArgumentParser()
    parser.add_argument("aln_input_file", \
    help = "file with blast/diamond matches expected format m8 \
    \nqueryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount,\
    queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore")
    parser.add_argument("query_length_file", \
    help = "tab delimited file of query lengths")
    parser.add_argument('-a','--acc_taxaid_mapping_file', \
    help = "mapping from accession to taxaid gzipped")
    parser.add_argument('-t','--taxonomy', \
    help = "path of taxdump.tar.gz extracted directory")
    parser.add_argument('-o','--output_file', type = str, \
    default = "taxonomyassignation", help = ("string specifying output file"))
    parser.add_argument('-i','--identity', default = 60, \
    help = "percentage of identity")
    parser.add_argument('-m','--max_matches', default = 10, \
    help = "max number of matches to analyze")
    parser.add_argument('-c','--min_coverage', default = 70, \
    help = "percentage of coverage")
    args = parser.parse_args()

    lengths = read_query_length_file(args.query_length_file)
    print("Finished reading lengths file")
    nb_total_prot = len(lengths)

    (matches,accs) = read_blast_input(args.aln_input_file, lengths, \
    args.identity,args.max_matches,args.min_coverage)
    print("Finished reading in blast results file")
    nb_prot_annotated = len(matches)

    mapping = map_accessions(accs, args.acc_taxaid_mapping_file)
    print("Finished loading taxaid map file")

    load_taxonomy(args.taxonomy)
    print("Finished loading taxa directory " + args.taxonomy)

    re_contig = re.compile('(.*)\.' + prot_prefix)
    with open (args.output_file + ".pergene.tsv", "w") as out, \
    open (args.output_file + ".percontig.tsv", "w") as outpercontig, \
    open (args.output_file + ".warn.tsv", "w") as outdisc :
        #Write header
        out.write("#prot_id\tconsensus_tax_id\tconsensus_lineage\ttax_id_by_level\n")
        outpercontig.write("#contig\tconsensus_tax_id\tconsensus_lineage\ttax_id_by_level\n")
        outdisc.write("#prot_id\tlist nr hit not found in taxo\n")

        collate_hits_per_contig = list()
        for depth in range(7):
            collate_hits_per_contig.append(Counter())

        count_genealogy = Counter()
        count_genealogy_contig = Counter()
        nb_prot_assigned = 0

        prev_contig = None
        contig_id = None
        for prot, matchs in list(matches.items()):

            hit_sorted = sorted(matchs, key = lambda x: x[1], reverse = True)
            ####
            # handle contig consensus
            match = re_contig.match(prot)
            if match :
                contig_id = match.group(1)

            if prev_contig == None :
                prev_contig = contig_id

            if prev_contig != contig_id :
                ###
                (tax_id_keep, fullnamelineage_text, fullnamelineage_ids) = get_consensus(collate_hits_per_contig)
                count_genealogy_contig[d_taxonomy[str(tax_id_keep)].level] += 1
                outpercontig.write(prev_contig + "\t" + str(tax_id_keep) + "\t" + fullnamelineage_text + "\t" + str(fullnamelineage_ids) + "\n")
                collate_hits_per_contig = list()
                for depth in range(7):
                    collate_hits_per_contig.append(Counter())
                prev_contig = contig_id

            best_hit = hit_sorted [0][0]
            fHit = hit_sorted [0][1]
            if mapping[best_hit] > -1:
                #Retrieve hit taxa id
                tax_id = mapping[best_hit]
                if str(tax_id) in d_taxonomy : # taxid in taxonomy ?
                    #Retreive lineage on main level only (no no rank)
                    hits = d_taxonomy[str(tax_id)].genealogy_main_level()
                    for depth in range(7):
                        if hits[depth] != "None":
                            weight = (fHit - MIN_IDENTITY_TAXA[depth]) / (1.0 - MIN_IDENTITY_TAXA[depth])
                            weight = max(weight,0.0)
                            if weight > 0.0:
                                collate_hits_per_contig[depth][hits[depth]] += weight #could put a transform in here
            # end handle contig consensus
            ####

            ####
            #Handle a protein, init variable
            added_matches = []
            collate_hits = list()
            protTaxaNotFound = list()
            protMatchNotFound = list()

            for depth in range(7):
                collate_hits.append(Counter())

            #For each hit, retrieve taxon id and compute weight in lineage
            for (match, fHit) in hit_sorted:
                #taxid id found in acc_taxaid_mapping_file
                if mapping[match] > -1:
                    #Retrieve hit taxa id
                    tax_id = mapping[match]
                    if tax_id not in added_matches:     # Only add the best hit per species
                        added_matches.append(tax_id)
                        if str(tax_id) in d_taxonomy : # taxid in taxonomy ?
                            #Retreive lineage on main level only (no no rank)
                            hits = d_taxonomy[str(tax_id)].genealogy_main_level()
                            for depth in range(7):
                                if hits[depth] != "None":
                                    weight = (fHit - MIN_IDENTITY_TAXA[depth]) / (1.0 - MIN_IDENTITY_TAXA[depth])
                                    weight = max(weight,0.0)
                                    if weight > 0.0:
                                        collate_hits[depth][hits[depth]] += weight #could put a transform in here
                        else :
                            if tax_id not in protTaxaNotFound:
                               protTaxaNotFound.append(tax_id)
                else :
                    if match not in protMatchNotFound:
                        protMatchNotFound.append(match)

            if len (added_matches) > 0 :
                (tax_id_keep, fullnamelineage_text, fullnamelineage_ids) = get_consensus(collate_hits)
                count_genealogy[d_taxonomy[str(tax_id_keep)].level] += 1
                #write simple output
                out.write(prot + "\t" + str(tax_id_keep) + "\t" + fullnamelineage_text + "\t" + str(fullnamelineage_ids) + "\n")
                nb_prot_assigned += 1

            #write discarded values.
            if len(protTaxaNotFound) > 0 :
                outdisc.write(prot + "\t" + "No taxid in taxdump\t" + ",".join(map(str, protTaxaNotFound)) + "\n")
            if len(protMatchNotFound) > 0 :
                outdisc.write(prot + "\t" + "No protid correspondance file\t" + ",".join(map(str, protMatchNotFound)) + "\n")

        if(os.path.getsize(args.aln_input_file) != 0):
            #handle last record of contigs consensus.
            (tax_id_keep, fullnamelineage_text, fullnamelineage_ids) = get_consensus(collate_hits_per_contig)
            count_genealogy_contig[d_taxonomy[str(tax_id_keep)].level] += 1
            outpercontig.write(prev_contig + "\t" + str(tax_id_keep) + "\t" + fullnamelineage_text + "\t" + str(fullnamelineage_ids) + "\n")

        #graphs
        try:
            os.makedirs("graphs")
        except OSError:
            if not os.path.isdir("graphs"):
                Raise

        # Sort dictionaries
        count_genealogy_ord = OrderedDict(sorted(count_genealogy.items(), key=lambda t: t[0]))
        count_genealogy_contig_ord = OrderedDict(sorted(count_genealogy_contig.items(), key=lambda t: t[0]))
        # Figures
        pyplot.bar(range(len(count_genealogy_ord.values())), count_genealogy_ord.values())
        pyplot.xticks(range(len(count_genealogy_ord.values())), count_genealogy_ord.keys())
        pyplot.xlabel("Taxonomy level")
        pyplot.ylabel("Number of proteins")
        pyplot.title(args.aln_input_file + " number of proteins at different taxonomy levels")
        pyplot.savefig("graphs/" + args.aln_input_file.split(sep="/")[-1] + "_prot_taxonomy_level.pdf")
        pyplot.close()
        pyplot.bar(range(len(count_genealogy_contig_ord.values())), count_genealogy_contig_ord.values())
        pyplot.xticks(range(len(count_genealogy_contig_ord.values())), count_genealogy_contig_ord.keys())
        pyplot.xlabel("Taxonomy level")
        pyplot.ylabel("Number of contigs")
        pyplot.title(args.aln_input_file + " number of contigs at different taxonomy levels")
        pyplot.savefig("graphs/" + args.aln_input_file.split(sep="/")[-1] + "_contig_taxonomy_level.pdf")
        pyplot.close()
        list_graphs = [nb_total_prot, nb_prot_annotated, nb_prot_assigned]
        pyplot.bar(range(len(list_graphs)), list_graphs)
        pyplot.xticks(range(len(list_graphs)), ["Total","Annotated","Assigned"])
        pyplot.ylabel("Number of proteins")
        pyplot.title(args.aln_input_file + " number of annotated and assigned proteins")
        pyplot.savefig("graphs/" + args.aln_input_file.split(sep="/")[-1] + "_nb_prot_annotated_and_assigned.pdf")
        pyplot.close()

if __name__ == "__main__":
    main(sys.argv[1:])
