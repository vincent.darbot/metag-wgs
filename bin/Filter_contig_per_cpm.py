#!/usr/bin/env python

"""----------------------------------------------------------------------------
  Script Name: Filter_contig_per_cpm.py
  Description: Calculates the CPM normalization of mapped reads for each \
               contig and returns contigs which have a CPM > cutoff in .fa.
  Input files: Samtools idxstats output file, .fasta file of contigs.
  Created By:  Joanna Fourquet
  Date:        2020-04-01
-------------------------------------------------------------------------------
"""

# Metadata
__author__ = 'Joanna Fourquet \
- GenPhySE - Team NED'
__copyright__ = 'Copyright (C) 2020 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

# Status: dev

# Modules importation

try:
    import argparse
    import sys
    import pandas as p
    import numpy as np
    from Bio.Seq import Seq
    from Bio.SeqRecord import SeqRecord
    import pprint
    from Bio import SeqIO
except ImportError as error:
    print(error)
    exit(1)

################################################
# Function
################################################

def cpm(counts):
    N = np.sum(counts.iloc[:,2], axis=0)
    C = counts.iloc[:,2]
    cpm_values = 1e6 * C / N
    return(cpm_values)

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--samtools_idxstats", \
    required = True, help = "samtools idxstats file containing contig id, \
    sequence length, number of mapped reads or fragments, \
    number of unmapped reads or fragments")
    parser.add_argument('-f', '--fasta_file', required = True, help = \
    'fasta file containing sequences of contigs.')
    parser.add_argument("-c", "--cutoff_cpm", required = True, \
    help = "Minimum number of reads in a contig")
    parser.add_argument("-s", "--select", \
    help = "Name of outpout .fa file containing contigs which passed cpm cutoff")
    parser.add_argument("-d", "--discard", \
    help = "Name of outpout .fa file containing contigs which don't passed cpm cutoff")
    args = parser.parse_args()

    # Read input table
    raw_counts = p.read_table(args.samtools_idxstats,sep ='\t',header = None, comment="*")

    # Calculates cpm for each contig
    res_cpm = cpm(raw_counts)

    cutoff = float(args.cutoff_cpm)
    # Contigs with nb reads > cutoff
    kept_contigs = raw_counts.iloc[np.where(res_cpm >= cutoff)[0],0]

    # Contigs with nb reads < cutoff
    unkept_contigs = raw_counts.iloc[np.where(res_cpm < cutoff)[0],0]

    # Write new fasta files with kept and unkept contigs
    with open(args.fasta_file, "rU") as fasta_file,\
        open(args.select, "w") as out_select_handle,\
        open(args.discard, "w") as out_discard_handle:
        for record in SeqIO.parse(fasta_file, "fasta"):
            try :
                contig_id = record.id
                if(contig_id in list(kept_contigs)):
                    SeqIO.write(record, out_select_handle, "fasta")
                else:
                    if(contig_id in list(unkept_contigs)):
                        SeqIO.write(record, out_discard_handle, "fasta")
            except :
                print ("Warning input fasta file: contig " + record.id + " issue")
                pass

if __name__ == "__main__":
    main(sys.argv[1:])
