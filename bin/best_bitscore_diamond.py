#!/usr/bin/env python

"""----------------------------------------------------------------------------
  Script Name: best_hit_diamond.py
  Description: Have best diamond hits for each gene/protein (best bitscore)
  Input files: Diamond output file (.m8)
  Created By:  Joanna Fourquet
  Date:        2021-01-13
-------------------------------------------------------------------------------
"""

# Metadata
__author__ = 'Joanna Fourquet \
- GenPhySE - NED'
__copyright__ = 'Copyright (C) 2021 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

# Status: dev

# Modules importation
try:
    import argparse
    import pandas as p
    import re
    import sys
    import os
    import operator
    from collections import defaultdict
    from collections import OrderedDict
    from collections import Counter
    from matplotlib import pyplot
except ImportError as error:
    print(error)
    exit(1)

def read_blast_input(blastinputfile):
    #c1.Prot_00001	EFK63346.1	100.0	85	0	0	1	85	62	146	1.6e-36	158.3	85	\
    # 146	EFK63346.1 LOW QUALITY PROTEIN: hypothetical protein HMPREF9008_04720, partial [Parabacteroides sp. 20_3]

    #queryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount,
    #queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore, queryLength, subjectLength, subjectTitle
    score = defaultdict(float)
    best_lines = defaultdict(list)
    nmatches = defaultdict(int);
    for line in open(blastinputfile):
        (queryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount, \
        queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore, queryLength, subjectLength, subjectTitle) \
        = line.rstrip().split("\t")
        if (nmatches[queryId] == 0):
            score[queryId] = float(bitScore)
            nmatches[queryId] += 1
            best_lines[queryId] = [line]
        else :
            if (nmatches[queryId] > 0 and float(bitScore) > score[queryId]):
                score[queryId] = float(bitScore)
                best_lines[queryId] = [line]
            else :
                if (nmatches[queryId] > 0 and float(bitScore) == score[queryId]):
                    best_lines[queryId].append(line)
    return(best_lines)

def main(argv):

    parser = argparse.ArgumentParser()
    parser.add_argument("aln_input_file", \
    help = "file with blast/diamond matches expected format m8 \
    \nqueryId, subjectId, percIdentity, alnLength, mismatchCount, gapOpenCount,\
    queryStart, queryEnd, subjectStart, subjectEnd, eVal, bitScore")
    parser.add_argument('-o','--output_file', type = str, \
    default = "best_hit.tsv", help = ("string specifying output file"))
    args = parser.parse_args()

    out_lines = read_blast_input(args.aln_input_file)
    with open (args.output_file, "w") as out :
        for id in out_lines.keys():
            for line in out_lines[id]:
                out.write(line)
    print("Finished")
    
if __name__ == "__main__":
    main(sys.argv[1:])
