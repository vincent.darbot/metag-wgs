#!/usr/bin/env python
import sys, re

#init dictionaries:
ref = ""
seqs = []

while 1 :
    line = sys.stdin.readline()
    #print line
    if line == '' :
        break
    else :
        if line[0] == ">":
            for seq in seqs :
                print(ref+"\t"+seq)
            ref = ""
            seqs = []
        else:
            a, b = line.split('>', 1)
            name = b.split("...")[0]
            rep = (line.rstrip()[-1] == '*')
            if rep :
                ref = name
                seqs.append(name)
            else :
                seqs.append(name)

for seq in seqs :
    print(ref+"\t"+seq)
